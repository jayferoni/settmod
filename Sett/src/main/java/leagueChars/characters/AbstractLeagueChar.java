package leagueChars.characters;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.CardLibrary.LibraryType;

import basemod.abstracts.CustomPlayer;
import basemod.animations.AbstractAnimation;
import leagueChars.cardPools.Nexus;

public abstract class AbstractLeagueChar extends CustomPlayer implements ILeagueChar {

  private final float PLAY_SOUND_CHANCE = 0.5f;
  AbstractLeagueChar(String name, PlayerClass playerClass, String[] orbTextures, String orbVfxPath, float[] layerSpeeds,
      AbstractAnimation animation) {
    super(name, playerClass, orbTextures, orbVfxPath, layerSpeeds, animation);
  }

  @Override
  public ArrayList<AbstractCard> getCardPool(ArrayList<AbstractCard> pool) {
    super.getCardPool(pool);
    List<LibraryType> libTypes = new ArrayList<>(getSharedCardPools());
    libTypes.add(Nexus.Enums.LIBRARY);
    List<AbstractCard> sharedCards = libTypes.stream().map(ch -> CardLibrary.getCardList(ch))
        .flatMap(cards -> cards.stream()).collect(Collectors.toList());
    sharedCards.stream().filter(c -> c.rarity != CardRarity.SPECIAL && c.rarity != CardRarity.BASIC)
        .forEach(c -> pool.add(c));
    return pool;
  }

  public void playCardSound(String cardSFX) {
    if (MathUtils.randomBoolean(PLAY_SOUND_CHANCE)) {
      AbstractDungeon.actionManager.addToBottom(new SFXAction(cardSFX));
    }
  }
  public void playCardSound(String cardSFX, float pitch) {
    if (MathUtils.randomBoolean(PLAY_SOUND_CHANCE)) {
      AbstractDungeon.actionManager.addToBottom(new SFXAction(cardSFX, pitch));
    }
  }
}
