package leagueChars.characters;

import leagueChars.relics.GloriousEvolution;
import basemod.abstracts.CustomPlayer;
import basemod.animations.SpriterAnimation;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.EnergyManager;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ScreenShake;
import com.megacrit.cardcrawl.helpers.CardLibrary.LibraryType;
import com.megacrit.cardcrawl.localization.CharacterStrings;
import com.megacrit.cardcrawl.screens.CharSelectInfo;
import com.megacrit.cardcrawl.unlock.UnlockTracker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import leagueChars.cards.Viktor.SiphonPower;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.Viktor.DeathRay;
import leagueChars.cards.Viktor.GravityField;
import leagueChars.cards.shared.AutoAttack;
import leagueChars.cards.shared.ClothArmor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static leagueChars.LeagueCharsMod.*;

public class Viktor extends AbstractLeagueChar {
  public static final Logger logger = LogManager.getLogger(Viktor.class.getName());

  public static class Enums {
    @SpireEnum
    public static AbstractPlayer.PlayerClass VIKTOR;
    @SpireEnum(name = "LEAGUE_CHARS_VIKTOR_COLOR") // These two HAVE to have the same absolutely identical name.
    public static AbstractCard.CardColor COLOR_DARK_RED;
    @SpireEnum(name = "LEAGUE_CHARS_VIKTOR_COLOR")
    @SuppressWarnings("unused")
    public static CardLibrary.LibraryType LIBRARY_COLOR;
  }

  // =============== CHARACTER ENUMERATORS =================

  // =============== BASE STATS =================

  public static final int ENERGY_PER_TURN = 3;
  public static final int STARTING_HP = 70;
  public static final int MAX_HP = 70;
  public static final int STARTING_GOLD = 99;
  public static final int CARD_DRAW = 5;
  public static final int ORB_SLOTS = 0;

  // =============== /BASE STATS/ =================

  // =============== STRINGS =================

  private static final String ID = makeID(Viktor.class.getSimpleName());
  private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString(ID);
  private static final String[] NAMES = characterStrings.NAMES;
  private static final String[] TEXT = characterStrings.TEXT;

  // =============== /STRINGS/ =================

  // =============== TEXTURES OF BIG ENERGY ORB ===============

  public static final String[] orbTextures = { "leagueCharsResources/images/char/defaultCharacter/orb/layer1.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer2.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer3.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer4.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer5.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer6.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer1d.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer2d.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer3d.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer4d.png",
      "leagueCharsResources/images/char/defaultCharacter/orb/layer5d.png", };

  // =============== /TEXTURES OF BIG ENERGY ORB/ ===============

  // =============== CHARACTER CLASS START =================

  public Viktor(String name, PlayerClass setClass) {
    super(name, setClass, orbTextures, "leagueCharsResources/images/char/defaultCharacter/orb/vfx.png", null,
        new SpriterAnimation("leagueCharsResources/images/char/defaultCharacter/Spriter/theDefaultAnimation.scml"));

    // =============== TEXTURES, ENERGY, LOADOUT =================

    initializeClass(VIKTOR_MODEL, VIKTOR_SHOULDER_2, VIKTOR_SHOULDER_1, VIKTOR_CORPSE, getLoadout(), 20.0F, -10.0F,
        220.0F, 290.0F, new EnergyManager(ENERGY_PER_TURN));

    // =============== /TEXTURES, ENERGY, LOADOUT/ =================

    // =============== ANIMATIONS =================

    // =============== /ANIMATIONS/ =================

    // =============== TEXT BUBBLE LOCATION =================

    dialogX = (drawX + 0.0F * Settings.scale); // set location for text bubbles
    dialogY = (drawY + 220.0F * Settings.scale); // you can just copy these values

    // =============== /TEXT BUBBLE LOCATION/ =================

  }

  // =============== /CHARACTER CLASS END/ =================

  // Starting description and loadout
  @Override
  public CharSelectInfo getLoadout() {
    return new CharSelectInfo(NAMES[0], TEXT[0], STARTING_HP, MAX_HP, ORB_SLOTS, STARTING_GOLD, CARD_DRAW, this,
        getStartingRelics(), getStartingDeck(), false);
  }

  // Starting Deck
  @Override
  public ArrayList<String> getStartingDeck() {
    ArrayList<String> retVal = new ArrayList<>();

    logger.info("Begin loading starter Deck Strings");

    retVal.add(SiphonPower.ID);
    retVal.add(GravityField.ID);
    retVal.add(DeathRay.ID);
    retVal.add(AutoAttack.ID);
    retVal.add(AutoAttack.ID);
    retVal.add(AutoAttack.ID);
    retVal.add(ClothArmor.ID);
    retVal.add(ClothArmor.ID);
    retVal.add(ClothArmor.ID);
    retVal.add(ClothArmor.ID);

    return retVal;
  }

  public ArrayList<String> getStartingRelics() {
    ArrayList<String> retVal = new ArrayList<>();

    retVal.add(GloriousEvolution.ID);
    UnlockTracker.markRelicAsSeen(GloriousEvolution.ID);

    return retVal;
  }

  // character Select screen effect
  @Override
  public void doCharSelectScreenSelectEffect() {
    CardCrawlGame.sound.playA("ViktorIntroSound", 0f); // Sound Effect
    CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.LOW, ScreenShake.ShakeDur.SHORT, false); // Screen Effect
  }

  // character Select on-button-press sound effect
  @Override
  public String getCustomModeCharacterButtonSoundKey() {
    return "ATTACK_DAGGER_1";
  }

  // Should return how much HP your maximum HP reduces by when starting a run at
  // Ascension 14 or higher. (ironclad loses 5, defect and silent lose 4 hp
  // respectively)
  @Override
  public int getAscensionMaxHPLoss() {
    return 5;
  }

  // Should return the card color enum to be associated with your character.
  @Override
  public AbstractCard.CardColor getCardColor() {
    return Enums.COLOR_DARK_RED;
  }

  // Should return a color object to be used to color the trail of moving cards
  @Override
  public Color getCardTrailColor() {
    return VIKTOR_DARK_RED;
  }

  // Should return a BitmapFont object that you can use to customize how your
  // energy is displayed from within the energy orb.
  @Override
  public BitmapFont getEnergyNumFont() {
    return FontHelper.energyNumFontRed;
  }

  // Should return class name as it appears in run history screen.
  @Override
  public String getLocalizedCharacterName() {
    return NAMES[0];
  }

  // Which card should be obtainable from the Match and Keep event?
  @Override
  public AbstractCard getStartCardForEvent() {
    return new DeathRay();
  }

  // The class name as it appears next to your player name in-game
  @Override
  public String getTitle(AbstractPlayer.PlayerClass playerClass) {
    return NAMES[1];
  }

  // Should return a new instance of your character, sending name as its name
  // parameter.
  @Override
  public AbstractPlayer newInstance() {
    return new Viktor(name, chosenClass);
  }

  // Should return a Color object to be used to color the miniature card images in
  // run history.
  @Override
  public Color getCardRenderColor() {
    return VIKTOR_DARK_RED;
  }

  // Should return a Color object to be used as screen tint effect when your
  // character attacks the heart.
  @Override
  public Color getSlashAttackColor() {
    return VIKTOR_DARK_RED;
  }

  // Should return an AttackEffect array of any size greater than 0. These effects
  // will be played in sequence as your character's finishing combo on the heart.
  // Attack effects are the same as used in DamageAction and the like.
  @Override
  public AbstractGameAction.AttackEffect[] getSpireHeartSlashEffect() {
    return new AbstractGameAction.AttackEffect[] { AbstractGameAction.AttackEffect.BLUNT_HEAVY,
        AbstractGameAction.AttackEffect.BLUNT_HEAVY, AbstractGameAction.AttackEffect.BLUNT_HEAVY };
  }

  @Override
  public void renderPlayerImage(SpriteBatch sb) {
    super.renderPlayerImage(sb);
  }

  // Should return a string containing what text is shown when your character is
  // about to attack the heart. For example, the defect is "NL You charge your
  // core to its maximum..."
  @Override
  public String getSpireHeartText() {
    return TEXT[1];
  }

  // The vampire events refer to the base game characters as "brother", "sister",
  // and "broken one" respectively.This method should return a String containing
  // the full text that will be displayed as the first screen of the vampires
  // event.
  @Override
  public String getVampireText() {
    return TEXT[2];
  }

  @Override
  public List<LibraryType> getSharedCardPools() {
    return Arrays.asList(AbilityPower.Enums.LIBRARY);
  }
}
