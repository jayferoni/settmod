package leagueChars.characters;

import java.util.List;

import com.megacrit.cardcrawl.helpers.CardLibrary.LibraryType;

public interface ILeagueChar {
  List<LibraryType> getSharedCardPools();
}
