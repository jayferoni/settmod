package leagueChars.powers;

import leagueChars.LeagueCharsMod;
import leagueChars.util.TextureLoader;
import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

import static leagueChars.LeagueCharsMod.makePowerPath;

//Gain 1 dex for the turn for each card played.

public class BloodlustPower extends AbstractPower implements CloneablePowerInterface {

    public static final String POWER_ID = LeagueCharsMod.makeID(BloodlustPower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;

    // We create 2 new textures *Using This Specific Texture Loader* - an 84x84
    // image and a 32x32 one.
    // There's a fallback "missing texture" image, so the game shouldn't crash if
    // you accidentally put a non-existent file.
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("Bloodlust84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("Bloodlust32.png"));
    private int oldStrength = 0;

    public BloodlustPower(final AbstractCreature owner) {

        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        type = PowerType.BUFF;
        isTurnBased = false;

        // We load those txtures here.
        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);
        this.amount = 0;
        updateDescription();
        this.recalculateStrength();
    }

    public void recalculateStrength() {
        double healthPct = (double) this.owner.currentHealth / (double) this.owner.maxHealth;
        int strength = 5 - (int) Math.floor(healthPct / 0.2);
        this.addToBot(new ApplyPowerAction(owner, owner, new StrengthPower(owner, strength - oldStrength)));
        this.oldStrength = strength;
        this.amount = oldStrength;
    }

    @Override
    public AbstractPower makeCopy() {
        return new BloodlustPower(owner);
    }

    @Override
    public void updateDescription() {
        this.description = DESCRIPTIONS[0];
    }
}
