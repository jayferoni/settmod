package leagueChars.powers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import basemod.interfaces.CloneablePowerInterface;
import leagueChars.LeagueCharsMod;
import leagueChars.util.TextureLoader;

public class InfinityEdgePower extends AbstractPower implements CloneablePowerInterface {

    private static final Logger logger = LogManager.getLogger(InfinityEdgePower.class.getSimpleName());
    public static final String POWER_ID = LeagueCharsMod.makeID(InfinityEdgePower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;

    // We create 2 new textures *Using This Specific Texture Loader* - an 84x84
    // image and a 32x32 one.
    // There's a fallback "missing texture" image, so the game shouldn't crash if
    // you accidentally put a non-existent file.
    private static final Texture tex84 = TextureLoader
            .getTexture(LeagueCharsMod.makePowerPath("InfinityEdge84.png"));
    private static final Texture tex32 = TextureLoader
            .getTexture(LeagueCharsMod.makePowerPath("InfinityEdge32.png"));

    public InfinityEdgePower(final AbstractCreature owner) {
        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        type = PowerType.BUFF;
        isTurnBased = false;

        // We load those txtures here.
        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);
        updateDescription();
    }

    public void modifyCritChance(int a) {
        this.amount += a;
    }

    @Override
    public AbstractPower makeCopy() {
        return new InfinityEdgePower(owner);
    }

    @Override
    public void updateDescription() {
        this.description = DESCRIPTIONS[0];
    }
}
