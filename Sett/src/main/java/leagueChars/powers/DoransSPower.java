package leagueChars.powers;

import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.shared.DoransShield;
import leagueChars.util.TextureLoader;

import java.util.Iterator;

import static leagueChars.LeagueCharsMod.makePowerPath;

//Gain 1 dex for the turn for each card played.

public class DoransSPower extends AbstractPower {
    public static final String POWER_ID = LeagueCharsMod.makeID(DoransSPower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;

    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("Shield84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("Shield32.png"));

    public DoransSPower(AbstractCreature owner, int amt) {
        this.name = powerStrings.NAME;
        this.ID = POWER_ID;
        this.owner = owner;
        this.amount = amt;
        this.updateDescription();
        this.loadRegion("accuracy");
        this.updateExistingDorans();
    }

    public void updateDescription() {
        this.description = DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1];
    }

    public void stackPower(int stackAmount) {
        this.fontScale = 8.0F;
        this.amount += stackAmount;
        this.updateExistingDorans();
    }

    private void updateExistingDorans() {
        Iterator var1 = AbstractDungeon.player.hand.group.iterator();

        AbstractCard c;
        while (var1.hasNext()) {
            c = (AbstractCard) var1.next();
            if (c instanceof DoransShield) {
                if (!c.upgraded) {
                    c.baseBlock = 3 + this.amount;
                } else {
                    c.baseBlock = 5 + this.amount;
                }
            }
        }

        var1 = AbstractDungeon.player.drawPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCard) var1.next();
            if (c instanceof DoransShield) {
                if (!c.upgraded) {
                    c.baseBlock = 3 + this.amount;
                } else {
                    c.baseBlock = 5 + this.amount;
                }
            }
        }

        var1 = AbstractDungeon.player.discardPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCard) var1.next();
            if (c instanceof DoransShield) {
                if (!c.upgraded) {
                    c.baseBlock = 3 + this.amount;
                } else {
                    c.baseBlock = 5 + this.amount;
                }
            }
        }

        var1 = AbstractDungeon.player.exhaustPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCard) var1.next();
            if (c instanceof DoransShield) {
                if (!c.upgraded) {
                    c.baseBlock = 3 + this.amount;
                } else {
                    c.baseBlock = 5 + this.amount;
                }
            }
        }

    }

    public void onDrawOrDiscard() {
        Iterator var1 = AbstractDungeon.player.hand.group.iterator();

        while (var1.hasNext()) {
            AbstractCard c = (AbstractCard) var1.next();
            if (c instanceof DoransShield) {
                if (!c.upgraded) {
                    c.baseBlock = 3 + this.amount;
                } else {
                    c.baseBlock = 5 + this.amount;
                }

            }
        }
    }
}