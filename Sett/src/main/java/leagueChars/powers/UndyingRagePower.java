package leagueChars.powers;

import static leagueChars.LeagueCharsMod.makePowerPath;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import basemod.interfaces.CloneablePowerInterface;
import leagueChars.LeagueCharsMod;
import leagueChars.util.TextureLoader;

//Gain 1 dex for the turn for each card played.

public class UndyingRagePower extends AbstractPower implements CloneablePowerInterface {

    public static final String POWER_ID = LeagueCharsMod.makeID(UndyingRagePower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;

    // We create 2 new textures *Using This Specific Texture Loader* - an 84x84
    // image and a 32x32 one.
    // There's a fallback "missing texture" image, so the game shouldn't crash if
    // you accidentally put a non-existent file.
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("UndyingRagePower84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("UndyingRagePower32.png"));

    private static final int HP_MIN_CAP = 25;

    public UndyingRagePower(final AbstractCreature owner) {
        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        type = PowerType.BUFF;
        isTurnBased = false;
        amount = 1;

        // We load those txtures here.
        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);
        updateDescription();
    }

    @Override
    public void updateDescription() {
        this.description = DESCRIPTIONS[0] + amount;
        if (amount == 1) {
            this.description += DESCRIPTIONS[2];
        } else {
            this.description += DESCRIPTIONS[1];
        }
    }

    @Override
    public void atStartOfTurn() {
        this.addToBot(new RemoveSpecificPowerAction(this.owner, this.owner, UndyingRagePower.POWER_ID));
    }

    @Override
    public float atDamageFinalReceive(float damage, DamageType type) {
        int currentHp = AbstractDungeon.player.currentHealth;
        if (currentHp <= HP_MIN_CAP) {
            return 0;
        }
        if (currentHp - damage < HP_MIN_CAP) {
            return currentHp - HP_MIN_CAP;
        }
        return damage;
    }

    @Override
    public AbstractPower makeCopy() {
        return new BloodlustPower(owner);
    }
}
