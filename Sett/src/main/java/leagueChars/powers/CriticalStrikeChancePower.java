package leagueChars.powers;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import basemod.interfaces.CloneablePowerInterface;
import leagueChars.LeagueCharsMod;
import leagueChars.util.AttackCritSubscriber;
import leagueChars.util.TextureLoader;

public class CriticalStrikeChancePower extends AbstractPower implements CloneablePowerInterface {

    public static final String POWER_ID = LeagueCharsMod.makeID(CriticalStrikeChancePower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;

    // We create 2 new textures *Using This Specific Texture Loader* - an 84x84
    // image and a 32x32 one.
    // There's a fallback "missing texture" image, so the game shouldn't crash if
    // you accidentally put a non-existent file.
    private static final Texture tex84 = TextureLoader
            .getTexture(LeagueCharsMod.makePowerPath("Crit84.png"));
    private static final Texture tex32 = TextureLoader
            .getTexture(LeagueCharsMod.makePowerPath("Crit32.png"));
    private List<AttackCritSubscriber> critSubscribers = new ArrayList<>();
    private List<AttackCritSubscriber> oneTimeSubscribers = new ArrayList<>();

    public CriticalStrikeChancePower(final AbstractCreature owner) {
        this(owner, 0);
    }

    public CriticalStrikeChancePower(final AbstractCreature owner, int amount) {
        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        type = PowerType.BUFF;
        isTurnBased = false;

        // We load those txtures here.
        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);
        this.amount = amount;
        updateDescription();
    }

    public void subscribeOnCrit(AttackCritSubscriber subscriber) {
        this.critSubscribers.add(subscriber);
    }

    public void unsubscribeOnCrit(AttackCritSubscriber subscriber) {
        this.critSubscribers.remove(subscriber);
    }

    public void subscribeOnCritOnce(AttackCritSubscriber subscriber) {
        this.oneTimeSubscribers.add(subscriber);
    }

    public void modifyCritChance(int a) {
        stackPower(a);
    }

    @Override
    public AbstractPower makeCopy() {
        return new CriticalStrikeChancePower(owner);
    }



    @Override
    public int onAttackToChangeDamage(DamageInfo info, int damageAmount) {
        double critChance = ((double) this.amount) / 100.0;
        boolean isCrit = Math.random() <= critChance;
        for (AttackCritSubscriber sub : this.critSubscribers) {
            sub.onCrit(isCrit);
        }
        for (AttackCritSubscriber sub : this.oneTimeSubscribers) {
            sub.onCrit(isCrit);
        }
        oneTimeSubscribers.clear();
        if (isCrit) {
            boolean hasInfinityEdge = AbstractDungeon.player.getPower(InfinityEdgePower.POWER_ID) != null;
            return (int) (damageAmount * (hasInfinityEdge ? 2.0f : 1.5f));
        }
        return damageAmount;
    }
    public void updateDescription() {
        description = DESCRIPTIONS[0] + amount + DESCRIPTIONS[1];
    }

    @Override
    public void stackPower(int stackAmount) {
        super.stackPower(Math.min(100 - this.amount, stackAmount));
    }
}
