package leagueChars.powers;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.VulnerablePower;

import leagueChars.LeagueCharsMod;
import leagueChars.util.TextureLoader;

import static leagueChars.LeagueCharsMod.makePowerPath;

public class DelayedVulnerablePower extends AbstractPower implements CloneablePowerInterface {

  public static final String POWER_ID = LeagueCharsMod.makeID(DelayedVulnerablePower.class.getSimpleName());
  private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
  public static final String NAME = powerStrings.NAME;
  public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;

  private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("Grasp84.png"));
  private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("Grasp32.png"));

  public DelayedVulnerablePower(final AbstractCreature owner, final int amount) {
    name = NAME;
    ID = POWER_ID;

    this.owner = owner;
    this.amount = amount;

    type = PowerType.BUFF;
    isTurnBased = false;

    // We load those txtures here.
    this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
    this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

    updateDescription();
  }

  @Override
  public void atStartOfTurn() {
    flash();
    for (AbstractMonster mo : AbstractDungeon.getCurrRoom().monsters.monsters) {
      this.addToBot(new ApplyPowerAction(mo, AbstractDungeon.player, new VulnerablePower(mo, this.amount, false),
          this.amount, true, AttackEffect.NONE));
    }
    this.addToBot(new RemoveSpecificPowerAction(this.owner, this.owner, DelayedVulnerablePower.POWER_ID));
  }

  @Override
  public void updateDescription() {
    description = DESCRIPTIONS[0] + amount + DESCRIPTIONS[1];
  }

  @Override
  public AbstractPower makeCopy() {
    return new DelayedVulnerablePower(owner, amount);
  }
}
