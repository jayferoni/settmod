package leagueChars.powers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.tryn.SpinningSlash;
import leagueChars.util.TextureLoader;

import static leagueChars.LeagueCharsMod.makePowerPath;

//Gain 1 dex for the turn for each card played.

public class BerserkersGreavesPower extends AbstractPower {
    public static final String POWER_ID = LeagueCharsMod.makeID("BerserkersGreavesPower");
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;



    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("Berserker84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("Berserker32.png"));
    private int counter;

    public BerserkersGreavesPower(AbstractCreature owner, int amount) {
        this.name = NAME;
        this.ID = POWER_ID;
        this.owner = owner;
        this.amount = amount;
        this.counter = 0;
        this.updateDescription();

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);
    }

    public void updateDescription() {
        this.description = DESCRIPTIONS[0];
    }

    public void onUseCard(AbstractCard card, UseCardAction action) {
        ++this.counter;
        if (this.counter == 3) {
            this.counter = 0;
            this.flash();
            this.addToBot(new GainEnergyAction(1));

        }

    }
    }

