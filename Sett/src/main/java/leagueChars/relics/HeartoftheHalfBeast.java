package leagueChars.relics;

import com.megacrit.cardcrawl.actions.common.HealAction;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.Sett.*;
import leagueChars.util.TextureLoader;
import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import basemod.abstracts.CustomRelic;

import static leagueChars.LeagueCharsMod.*;

public class HeartoftheHalfBeast extends CustomRelic {

    /*
     * https://github.com/daviscook477/BaseMod/wiki/Custom-Relics
     *
     * Gain 1 energy.
     */

    // ID, images, text.
    public static final String ID = LeagueCharsMod.makeID("HeartoftheHalfBeast");

    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("Grit.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    public HeartoftheHalfBeast() {
        super(ID, IMG, OUTLINE, RelicTier.STARTER, LandingSound.MAGICAL);
        this.counter = 0;
    }

    // Flash at the start of Battle.
    @Override
    public void atBattleStartPreDraw() {
        flash();
        this.counter -= this.counter / 3;
    }

    @Override
    public int onAttacked(DamageInfo info, int damageAmount) {
        this.counter += info.output;
        if (this.counter > 100)
            this.counter = 100;
        return super.onAttacked(info, damageAmount);
    }

    @Override
    public void onUseCard(AbstractCard targetCard, UseCardAction useCardAction) {
        if (targetCard.cardID.equals(Haymaker.ID)) {
            AbstractDungeon.actionManager
                    .addToBottom(new GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.counter / 2));
            AbstractDungeon.actionManager.addToBottom(new DamageAllEnemiesAction(AbstractDungeon.player,
                    this.counter / 2, DamageInfo.DamageType.NORMAL, AbstractGameAction.AttackEffect.BLUNT_HEAVY));
            this.counter = 0;
        }
        if (targetCard.cardID.equals(AgainsttheOdds.ID)) {
            this.counter += AbstractDungeon.player.maxHealth - AbstractDungeon.player.currentHealth;
        }
        if (targetCard.cardID.equals(TradingBlow.ID)) {
            this.counter += 15;
        }
        if (targetCard.cardID.equals(SitUps.ID)) {
            {
                this.addToBot(new HealAction(AbstractDungeon.player, AbstractDungeon.player, this.counter / 10));
                this.counter -= this.counter / 3;
            }
        }
    }

    // Description
    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }

}
