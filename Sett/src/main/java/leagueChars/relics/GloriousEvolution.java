package leagueChars.relics;

import com.badlogic.gdx.graphics.Texture;
import com.evacipated.cardcrawl.mod.stslib.relics.ClickableRelic;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.LocalizedStrings;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import com.megacrit.cardcrawl.vfx.UpgradeShineEffect;
import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
import com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect;

import basemod.abstracts.CustomRelic;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.Viktor.ChaosStorm;
import leagueChars.cards.Viktor.DeathRay;
import leagueChars.cards.Viktor.EmpoweredDeathRay;
import leagueChars.cards.Viktor.EmpoweredGravityField;
import leagueChars.cards.Viktor.EmpoweredSiphonPower;
import leagueChars.cards.Viktor.GravityField;
import leagueChars.cards.Viktor.SiphonPower;
import leagueChars.util.TextureLoader;

import static leagueChars.LeagueCharsMod.*;

import java.util.HashMap;
import java.util.Map;

public class GloriousEvolution extends CustomRelic implements ClickableRelic {

  // ID, images, text.
  public static final String ID = LeagueCharsMod.makeID(GloriousEvolution.class.getSimpleName());

  private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("GloriousRevolution.png"));
  private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

  private int relicLevel = 1;
  private boolean isEmpoweredSiphonPower = false;
  private boolean isEmpoweredGravityField = false;
  private boolean isEmpoweredDeathRay = false;
  private boolean isChoosingUpgrade = false;

  private int currentGold = 0;
  private boolean alreadyGainedGold = false;

  private static final Map<Integer, Integer> GOLD_REQUIREMENTS = new HashMap<>();
  static {
    GOLD_REQUIREMENTS.put(1, 150);
    GOLD_REQUIREMENTS.put(2, 450);
    GOLD_REQUIREMENTS.put(3, 900);
  }

  public GloriousEvolution() {
    super(ID, IMG, OUTLINE, RelicTier.STARTER, LandingSound.MAGICAL);
    this.counter = 0;
  }

  @Override
  public void onEquip() {
    this.currentGold = AbstractDungeon.player.gold;
  }

  @Override
  public void onGainGold() {
    if (alreadyGainedGold || !(AbstractDungeon.getCurrRoom() != null
        && AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMPLETE)) {
      alreadyGainedGold = false;
      return;
    }
    logger.info((int) ((AbstractDungeon.player.gold - currentGold) * 0.20f));
    alreadyGainedGold = true;
    AbstractDungeon.player.gainGold((int) ((AbstractDungeon.player.gold - currentGold) * 0.20f));
    this.currentGold = AbstractDungeon.player.gold;
  }

  @Override
  public void onRightClick() {
    if (!isObtained
        || AbstractDungeon.getCurrRoom() != null && AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT
        || this.relicLevel == 4 || AbstractDungeon.player.gold == 0) {
      return;
    }
    int goldToSpend = Math.min(GOLD_REQUIREMENTS.get(this.relicLevel) - this.counter, AbstractDungeon.player.gold);
    this.counter += goldToSpend;
    AbstractDungeon.player.loseGold(goldToSpend);
    if (GOLD_REQUIREMENTS.get(this.relicLevel) == this.counter) {
      this.relicLevel += 1;
      CardGroup group = new CardGroup(CardGroupType.UNSPECIFIED);
      if (!isEmpoweredSiphonPower) {
        group.addToBottom(new SiphonPower());
      }
      if (!isEmpoweredGravityField) {
        group.addToBottom(new GravityField());
      }
      if (!isEmpoweredDeathRay) {
        group.addToBottom(new DeathRay());
      }
      this.isChoosingUpgrade = true;
      if (AbstractDungeon.isScreenUp) {
        AbstractDungeon.dynamicBanner.hide();
        AbstractDungeon.overlayMenu.cancelButton.hide();
        AbstractDungeon.previousScreen = AbstractDungeon.screen;
      }

      AbstractDungeon.gridSelectScreen.open(group, 1, this.DESCRIPTIONS[1] + LocalizedStrings.PERIOD, false, false,
          false, false);
    }
  }

  @Override
  public void update() {
    super.update();
    if (this.isChoosingUpgrade && !AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
      this.isChoosingUpgrade = false;
      AbstractCard card = AbstractDungeon.gridSelectScreen.selectedCards.get(0);
      logger.info(card.cardID);
      if (card.cardID.equals(SiphonPower.ID)) {
        empowerSiphonPower();
      }
      if (card.cardID.equals(GravityField.ID)) {
        empowerGravityField();
      }
      if (card.cardID.equals(DeathRay.ID)) {
        empowerDeathRay();
      }
      if (isEmpoweredSiphonPower && isEmpoweredGravityField && isEmpoweredDeathRay) {
        AbstractDungeon.topLevelEffects.add(
            new ShowCardAndObtainEffect(new ChaosStorm(), (float) (Settings.WIDTH / 2), (float) (Settings.HEIGHT / 2)));
      }
      AbstractDungeon.gridSelectScreen.selectedCards.clear();
    }
  }

  // Description
  @Override
  public String getUpdatedDescription() {
    return DESCRIPTIONS[0];
  }

  @Override
  public void onMasterDeckChange() {
    if (isEmpoweredDeathRay) {
      replaceDeathRays();
    }
    if (isEmpoweredSiphonPower) {
      replaceSiphonPowers();
    }
    if (isEmpoweredGravityField) {
      replaceGravityFields();
    }
  }

  private long replaceSiphonPowers() {
    long normalCount = AbstractDungeon.player.masterDeck.group.stream()
        .filter(c -> c.cardID.equals(SiphonPower.ID) && c.canUpgrade()).count();
    long upgradeCount = AbstractDungeon.player.masterDeck.group.stream()
        .filter(c -> c.cardID.equals(SiphonPower.ID) && c.upgraded).count();
    AbstractDungeon.player.masterDeck.removeCard(SiphonPower.ID);
    for (int i = 0; i < normalCount; i++) {
      AbstractDungeon.player.masterDeck.addToTop(new EmpoweredSiphonPower());
    }
    for (int i = 0; i < upgradeCount; i++) {
      EmpoweredSiphonPower card = new EmpoweredSiphonPower();
      card.upgrade();
      AbstractDungeon.player.masterDeck.addToTop(card);
    }
    return normalCount + upgradeCount;
  }

  private long replaceGravityFields() {
    long normalCount = AbstractDungeon.player.masterDeck.group.stream()
        .filter(c -> c.cardID.equals(GravityField.ID) && c.canUpgrade()).count();
    long upgradeCount = AbstractDungeon.player.masterDeck.group.stream()
        .filter(c -> c.cardID.equals(GravityField.ID) && c.upgraded).count();
    AbstractDungeon.player.masterDeck.removeCard(GravityField.ID);
    for (int i = 0; i < normalCount; i++) {
      AbstractDungeon.player.masterDeck.addToTop(new EmpoweredGravityField());
    }
    for (int i = 0; i < upgradeCount; i++) {
      EmpoweredGravityField card = new EmpoweredGravityField();
      card.upgrade();
      AbstractDungeon.player.masterDeck.addToTop(card);
    }
    return normalCount + upgradeCount;
  }

  private long replaceDeathRays() {
    long normalCount = AbstractDungeon.player.masterDeck.group.stream()
        .filter(c -> c.cardID.equals(DeathRay.ID) && c.canUpgrade()).count();
    long upgradeCount = AbstractDungeon.player.masterDeck.group.stream()
        .filter(c -> c.cardID.equals(DeathRay.ID) && c.upgraded).count();
    AbstractDungeon.player.masterDeck.removeCard(DeathRay.ID);
    for (int i = 0; i < normalCount; i++) {
      AbstractDungeon.player.masterDeck.addToTop(new EmpoweredDeathRay());
    }
    for (int i = 0; i < upgradeCount; i++) {
      EmpoweredDeathRay card = new EmpoweredDeathRay();
      card.upgrade();
      AbstractDungeon.player.masterDeck.addToTop(card);
    }
    return normalCount + upgradeCount;
  }

  private void empowerSiphonPower() {
    this.isEmpoweredSiphonPower = true;
    if (replaceSiphonPowers() > 0) {
      AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect((new EmpoweredSiphonPower())));
      AbstractDungeon.topLevelEffects
          .add(new UpgradeShineEffect((float) Settings.WIDTH / 2.0F, (float) Settings.HEIGHT / 2.0F));
    }
  }

  private void empowerGravityField() {
    this.isEmpoweredGravityField = true;
    if (replaceGravityFields() > 0) {
      AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect((new EmpoweredGravityField())));
      AbstractDungeon.topLevelEffects
          .add(new UpgradeShineEffect((float) Settings.WIDTH / 2.0F, (float) Settings.HEIGHT / 2.0F));
    }
  }

  private void empowerDeathRay() {
    this.isEmpoweredDeathRay = true;
    if (replaceDeathRays() > 0) {
      AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect((new EmpoweredDeathRay())));
      AbstractDungeon.topLevelEffects
          .add(new UpgradeShineEffect((float) Settings.WIDTH / 2.0F, (float) Settings.HEIGHT / 2.0F));
    }
  }

}
