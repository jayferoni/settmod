package leagueChars.relics;

import static leagueChars.LeagueCharsMod.makeRelicOutlinePath;
import static leagueChars.LeagueCharsMod.makeRelicPath;

import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import basemod.abstracts.CustomRelic;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.tryn.UndyingRage;
import leagueChars.characters.Tryndamere;
import leagueChars.powers.BloodlustPower;
import leagueChars.powers.CriticalStrikeChancePower;
import leagueChars.util.TextureLoader;

public class BattleFury extends CustomRelic {

    public static final String ID = LeagueCharsMod.makeID(BattleFury.class.getSimpleName());

    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("BattleFury.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    private boolean inCombat = false;
    private int oldCrit = 0;

    public BattleFury() {
        super(ID, IMG, OUTLINE, RelicTier.STARTER, LandingSound.MAGICAL);
        this.counter = 0;
    }

    public int getFury() {
        return this.counter;
    }

    public void addFury(int amount) {
        this.counter += amount;
        this.counter = Math.min(this.counter, 100);
        this.recalculateCritChance();
    }

    public void resetFury() {
        this.counter = 0;
        this.recalculateCritChance();
    }

    private void recalculateBonusAd() {
        Tryndamere tryn = (Tryndamere) AbstractDungeon.player;
        tryn.getBloodlustPower().recalculateStrength();
    }

    private void recalculateCritChance() {
        Tryndamere tryn = (Tryndamere) AbstractDungeon.player;
        int newCritChance = (int) Math.floor(this.counter * 0.3);
        CriticalStrikeChancePower crit = tryn.getCritChancePower();
        int critChanceDiff = newCritChance - oldCrit;
        crit.modifyCritChance(critChanceDiff);
        this.oldCrit = newCritChance;
    }

    // Flash at the start of Battle.
    @Override
    public void atBattleStartPreDraw() {
        flash();
        this.counter = 0;
        this.oldCrit = 0;
        this.addToBot(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player,
                new BloodlustPower(AbstractDungeon.player)));
        CriticalStrikeChancePower critChance = new CriticalStrikeChancePower(AbstractDungeon.player);
        critChance.subscribeOnCrit((crit) -> {
            if (crit) {
                this.addFury(10);
            } else {
                this.addFury(5);
            }
        });
        this.addToBot(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, critChance));
    }

    @Override
    public void onUseCard(AbstractCard targetCard, UseCardAction useCardAction) {
        if (targetCard.cardID.equals(UndyingRage.ID)) {
            this.addFury(50);
        }
    }

    @Override
    public void onMonsterDeath(AbstractMonster monster) {
        this.addFury(10);
    }

    @Override
    public void onLoseHp(int damageAmount) {
        if (inCombat) {
            recalculateBonusAd();
        }
    }

    @Override
    public void atBattleStart() {
        this.inCombat = true;
    }

    @Override
    public void onVictory() {
        this.inCombat = false;
    }

    // Description
    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }

}
