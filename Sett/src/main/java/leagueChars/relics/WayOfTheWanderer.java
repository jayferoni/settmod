package leagueChars.relics;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.Mordekaiser.Indestructible;
import leagueChars.cards.Yasuo.LastBreath;
import leagueChars.cards.Yasuo.SweepingBlade;
import leagueChars.powers.LastBreathPower;
import leagueChars.util.TextureLoader;
import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import basemod.abstracts.CustomRelic;

import static leagueChars.LeagueCharsMod.*;

public class WayOfTheWanderer extends CustomRelic {

    /*
     * https://github.com/daviscook477/BaseMod/wiki/Custom-Relics
     *
     * Gain 1 energy.
     */

    // ID, images, text.
    public static final String ID = LeagueCharsMod.makeID("WayOfTheWanderer");

    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("WayWanderer.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    public WayOfTheWanderer() {
        super(ID, IMG, OUTLINE, RelicTier.STARTER, LandingSound.MAGICAL);
        this.counter = 0;
    }

    // Flash at the start of Battle.
    @Override
    public void atBattleStartPreDraw() {
        flashWhenMax();
    }

    @Override
    public int onAttacked(DamageInfo info, int damageAmount) {
        if (this.counter == 100) {
            this.counter = 0;
            AbstractDungeon.actionManager
                    .addToBottom(new GainBlockAction(AbstractDungeon.player, AbstractDungeon.player,
                            AbstractDungeon.player.maxHealth / 2));
            AbstractDungeon.actionManager
                    .addToBottom(new DamageAction(AbstractDungeon.player, info, AbstractGameAction.AttackEffect.NONE));
            return 0;
        }
        return super.onAttacked(info, damageAmount);
    }

    @Override
    public void onCardDraw(AbstractCard drawnCard) {
        super.onCardDraw(drawnCard);
        this.counter = Math.min(this.counter + 5, 100);
        flashWhenMax();
    }

    @Override
    public void onUseCard(AbstractCard targetCard, UseCardAction useCardAction) {
        if (targetCard.cardID.equals(SweepingBlade.ID)) {
            this.counter = Math.min(this.counter + 20, 100);
            flashWhenMax();
        }
        if (targetCard.cardID.equals(LastBreath.ID)) {
            if (AbstractDungeon.getMonsters().monsters.stream()
                    .filter(m -> m.getPower(LastBreathPower.POWER_ID) != null).count() > 0) {
                this.counter = 100;
                flash();
            }
        }
    }

    // Description
    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }

    private void flashWhenMax() {
        if (this.counter == 100) {
            flash();
        }
    }

}
