package leagueChars.relics;

import basemod.abstracts.CustomRelic;
import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.StrengthPower;
import leagueChars.LeagueCharsMod;
import leagueChars.util.TextureLoader;

import static leagueChars.LeagueCharsMod.makeRelicOutlinePath;
import static leagueChars.LeagueCharsMod.makeRelicPath;

public class WinnerofthePit extends CustomRelic {
    /*
     * https://github.com/daviscook477/BaseMod/wiki/Custom-Relics
     *
     * At the start of each combat, gain 1 Strength (i.e. Vajra)
     */

    // ID, images, text.
    public static final String ID = LeagueCharsMod.makeID("WinnerofthePit");

    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("WinnerofthePit.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic2.png"));

    public WinnerofthePit() {
        super(ID, IMG, OUTLINE, RelicTier.UNCOMMON, LandingSound.FLAT);
        this.counter = 0;

    }

    @Override
    public void onVictory() {
        this.counter++;
    }

    // Gain 1 Strength on on equip.
    @Override
    public void atBattleStart() {
        flash();

        this.addToBot(new LoseHPAction(AbstractDungeon.player, AbstractDungeon.player, this.counter / 2));
        AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player,
                new StrengthPower(AbstractDungeon.player, this.counter / 2), this.counter / 2));
        AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));

    }

    // Description
    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }

}
