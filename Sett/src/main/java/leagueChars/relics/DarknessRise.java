package leagueChars.relics;

import basemod.abstracts.CustomRelic;
import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.cards.status.Wound;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.Mordekaiser.Indestructible;
import leagueChars.cards.Mordekaiser.IndestructibleRecast;
import leagueChars.cards.Sett.*;
import leagueChars.characters.Mordekaiser;
import leagueChars.util.TextureLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static leagueChars.LeagueCharsMod.makeRelicOutlinePath;
import static leagueChars.LeagueCharsMod.makeRelicPath;

public class DarknessRise extends CustomRelic {

    /*
     * https://github.com/daviscook477/BaseMod/wiki/Custom-Relics
     *
     * Gain 1 energy.
     */

    // ID, images, text.
    public static final Logger logger = LogManager.getLogger(DarknessRise.class.getName());
    public static final String ID = LeagueCharsMod.makeID("DarknessRise");

    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("DarknessRise.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));
    private int passiveCounter;
    private boolean indestructiblePlayed;
    public DarknessRise() {
        super(ID, IMG, OUTLINE, RelicTier.STARTER, LandingSound.MAGICAL);
        this.counter = 0;

        this.indestructiblePlayed=false;


    }


    // Flash at the start of Battle.
    @Override
    public void atBattleStartPreDraw() {
        flash();
        this.counter = 0;


    }

    @Override
    public void onAttack(DamageInfo info, int damageAmount, AbstractCreature target) {
        this.counter+=info.output/3;
        if (this.counter > 50) this.counter = 50;
        super.onAttack(info, damageAmount, target);
    }

    @Override
    public void atTurnStart() {
        this.addToBot(new MakeTempCardInHandAction(new Indestructible(), 1));
    }

    @Override
    public void onUseCard(AbstractCard card, UseCardAction useCardAction) {

        if (card.cardID.equals(Indestructible.ID)) {
            indestructiblePlayed=true;
            AbstractDungeon.actionManager.addToBottom(new GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.counter ));
        }
        if (card.cardID.equals(IndestructibleRecast.ID)) {
            AbstractDungeon.actionManager.addToBottom(new RemoveAllBlockAction(AbstractDungeon.player, AbstractDungeon.player));
            AbstractDungeon.actionManager.addToBottom(new HealAction(AbstractDungeon.player, AbstractDungeon.player, this.counter / 5));
            this.counter=0;
            this.indestructiblePlayed = false;

        }
logger.info("passiveCounter: " + passiveCounter + ",counter: " + counter);
    }

    @Override
    public void onPlayerEndTurn() {

        if (indestructiblePlayed) {
            this.counter = 0;
            this.indestructiblePlayed = false;

        }
        else {
            this.counter -= this.counter / 3;
        }
    }
    // Description
    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }

}
