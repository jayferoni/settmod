package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class VoidStaff extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(VoidStaff.class.getSimpleName());
    public static final String IMG = makeCardPath("VoidStaff.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;
    private static final int AP_SCALING = 30;
    private static final int UPGRADE_AP_SCALING = 10;
    private static final int DAMAGE = 12;
    private static final int UPGRADE_PLUS_DMG = 2;

    public VoidStaff() {
        super(ID, IMG, 2, CardType.ATTACK, COLOR, CardRarity.UNCOMMON, CardTarget.ENEMY);
        this.baseDamage = DAMAGE;
        this.baseMagicNumber = 5;
        this.magicNumber = this.baseMagicNumber;

        setAbilityPowerScaling(AP_SCALING);

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_VERTICAL));
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));





    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
            upgradeDamage(UPGRADE_PLUS_DMG);
            this.upgradeMagicNumber(5);



        }

    }

}
