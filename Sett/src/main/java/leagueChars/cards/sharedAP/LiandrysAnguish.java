package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LiandrysAnguish extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(LiandrysAnguish.class.getSimpleName());
    public static final String IMG = makeCardPath("LiandrysAnguish.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    public LiandrysAnguish() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseMagicNumber = 8;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));

        this.addToBot(new GainEnergyAction(2));


    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(4);



        }

    }

}
