package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cardPools.AttackDamage;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.cards.shared.DoransBlade;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DoransAPBuild extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DoransAPBuild.class.getSimpleName());
    public static final String IMG = makeCardPath("DoransAPBuild.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    public DoransAPBuild() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        this.addToBot(new MakeTempCardInHandAction(new DoransRing(), 3));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(1);

        }

    }

}
