package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class RabadonsDeathcap extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(RabadonsDeathcap.class.getSimpleName());
    public static final String IMG = makeCardPath("RabadonsDeathcap.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    public RabadonsDeathcap() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.baseMagicNumber = 45;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));



    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(10);





        }

    }

}
