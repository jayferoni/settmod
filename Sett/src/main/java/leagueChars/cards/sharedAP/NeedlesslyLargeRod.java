package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class NeedlesslyLargeRod extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(NeedlesslyLargeRod.class.getSimpleName());
    public static final String IMG = makeCardPath("NeedlesslyLargeRod.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    public NeedlesslyLargeRod() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.SELF);
        this.baseMagicNumber = 10;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));



    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(5);





        }

    }

}
