package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.PummelDamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.vfx.combat.CleaveEffect;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class NashoorsTooth extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(NashoorsTooth.class.getSimpleName());
  public static final String IMG = makeCardPath("NashorsTooth.png");

  private static final CardRarity RARITY = CardRarity.UNCOMMON;
  private static final CardTarget TARGET = CardTarget.ENEMY;
  private static final CardType TYPE = CardType.ATTACK;
  public static final CardColor COLOR = AbilityPower.Enums.COLOR;

  private static final int COST = 2;

  private static final int DAMAGE = 4;
  private static final int UPGRADE_PLUS_DMG = 1;
  private static final int AP_SCALING = 15;
  private static final int UPGRADE_AP_SCALING = 10;

  public NashoorsTooth() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseDamage = DAMAGE;
    this.baseMagicNumber = 3;
    this.magicNumber = this.baseMagicNumber;
    this.exhaust=true;

    setAbilityPowerScaling(AP_SCALING);
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    for (int i = 1; i < this.magicNumber; ++i) {
      this.addToBot(new PummelDamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn)));
    }

    this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn),
            AbstractGameAction.AttackEffect.BLUNT_HEAVY));


    this.addToBot(new VFXAction(p, new CleaveEffect(), 0.1F));
    this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,5),5));
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      upgradeDamage(UPGRADE_PLUS_DMG);
      initializeDescription();
      upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
    }
  }
}
