package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.actions.DarkHarvestAction;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DarkHarvest extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DarkHarvest.class.getSimpleName());
    public static final String IMG = makeCardPath("DarkHarvest.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    private static final int AP_SCALING = 40;
    private static final int UPGRADE_AP_SCALING = 20;
    private static final int DAMAGE = 1;
    private static final int UPGRADE_PLUS_DMG = 1;

    public DarkHarvest() {
        super(ID, IMG, 1, CardType.ATTACK, COLOR, CardRarity.RARE, CardTarget.ENEMY);
        this.baseDamage = DAMAGE;
        this.baseMagicNumber = 30;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust=true;

        setAbilityPowerScaling(AP_SCALING);

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new DarkHarvestAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), this.magicNumber));




    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
            upgradeDamage(UPGRADE_PLUS_DMG);
            this.upgradeMagicNumber(15);



        }

    }

}
