package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class SeekersArmguard extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(SeekersArmguard.class.getSimpleName());
    public static final String IMG = makeCardPath("SeekersArmguard.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    public SeekersArmguard() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.SELF);
        this.baseMagicNumber = 5;
        this.magicNumber = this.baseMagicNumber;
        this.baseBlock = 8;

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p, this.magicNumber), this.magicNumber));
        this.addToBot(new GainBlockAction(p, p, this.block));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(2);
            this.upgradeBlock(2);

        }

    }

}
