package leagueChars.cards.sharedAP;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DoransRing extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DoransRing.class.getSimpleName());
    public static final String IMG = makeCardPath("DoransRing.png");
    public static final CardColor COLOR = AbilityPower.Enums.COLOR;

    public DoransRing() {
        super(ID, IMG, 0, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.SELF);
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p, this.magicNumber), this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(2);
        }

    }

}
