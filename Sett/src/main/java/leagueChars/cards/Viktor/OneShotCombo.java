package leagueChars.cards.Viktor;

import static leagueChars.LeagueCharsMod.makeCardPath;

import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;

public class OneShotCombo extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(OneShotCombo.class.getSimpleName());
    public static final String IMG = makeCardPath("OneShotCombo.png");
    public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

    private static final int UPGRADE_COST = 1;

    public OneShotCombo() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);

        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;
        this.cardsToPreview = new SiphonPower();
        this.cardsToPreview = new DeathRay();


    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        AbstractCard c = new SiphonPower() ;
        c.setCostForTurn(0);
        this.addToBot(new MakeTempCardInHandAction(c, 1));
        AbstractCard d = new DeathRay() ;
        d.setCostForTurn(0);
        this.addToBot(new MakeTempCardInHandAction(d, 1));



    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            upgradeBaseCost(UPGRADE_COST);




        }

    }

}
