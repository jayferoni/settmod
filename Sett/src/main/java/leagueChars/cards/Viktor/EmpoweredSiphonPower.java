package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;
import com.megacrit.cardcrawl.powers.LoseDexterityPower;

import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Viktor;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;

public class EmpoweredSiphonPower extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(EmpoweredSiphonPower.class.getSimpleName());
  public static final String IMG = LeagueCharsMod.makeCardPath("EmpoweredSiphonPower.png");

  private static final CardRarity RARITY = CardRarity.SPECIAL;
  private static final CardTarget TARGET = CardTarget.ENEMY;
  private static final CardType TYPE = CardType.ATTACK;
  public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

  private static final int COST = 1;

  private static final int DAMAGE = 4;
  private static final int UPGRADE_PLUS_DMG = 2;
  private static final int BLOCK = 4;
  private static final int UPGRADE_PLUS_BLOCK = 1;
  private static final int MAGIC_NUMBER = 3;
  private static final int UPGRADE_MAGIC_NUMBER = 1;

  public EmpoweredSiphonPower() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseDamage = DAMAGE;
    this.baseBlock = BLOCK;
    this.baseMagicNumber = MAGIC_NUMBER;
    this.magicNumber = baseMagicNumber;
    this.cardsToPreview = new Discharge();
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    if (p instanceof AbstractLeagueChar) {
      ((AbstractLeagueChar) p).playCardSound("ViktorSiphonPower", 0.05f);
    }
    this.addToBot(new ApplyPowerAction(p, p, new DexterityPower(p, this.magicNumber), this.magicNumber));
    this.addToBot(new ApplyPowerAction(p, p, new LoseDexterityPower(p, this.magicNumber), this.magicNumber));
    this.addToBot(new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn),
        AbstractGameAction.AttackEffect.SLASH_VERTICAL));
    this.addToBot(new GainBlockAction(p, this.block));
    Discharge discharge = new Discharge();
    if (this.upgraded) {
      discharge.upgrade();
    }
    this.addToBot(new MakeTempCardInHandAction(discharge));
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      upgradeDamage(UPGRADE_PLUS_DMG);
      upgradeBlock(UPGRADE_PLUS_BLOCK);
      upgradeMagicNumber(UPGRADE_MAGIC_NUMBER);
      initializeDescription();
    }
  }
}
