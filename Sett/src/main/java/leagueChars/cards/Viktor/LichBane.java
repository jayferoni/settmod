package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LichBane extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(LichBane.class.getSimpleName());
    public static final String IMG = makeCardPath("LichBane.png");
    public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;
    private static final int AP_SCALING = 50;
    private static final int UPGRADE_AP_SCALING = 10;
    private static final int DAMAGE = 2;
    private static final int UPGRADE_PLUS_DMG = 2;

    public LichBane() {
        super(ID, IMG, 2, CardType.ATTACK, COLOR, CardRarity.COMMON, CardTarget.ENEMY);
        this.baseDamage = DAMAGE;

        setAbilityPowerScaling(AP_SCALING);

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_VERTICAL));




    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
            upgradeDamage(UPGRADE_PLUS_DMG);


        }

    }

}
