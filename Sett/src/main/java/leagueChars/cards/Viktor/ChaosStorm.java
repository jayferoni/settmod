package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Viktor;
import leagueChars.powers.ChaosStormPower;

public class ChaosStorm extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(ChaosStorm.class.getSimpleName());
  public static final String IMG = LeagueCharsMod.makeCardPath("ChaosStorm.png");
  public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);

  private static final CardRarity RARITY = CardRarity.SPECIAL;
  private static final CardTarget TARGET = CardTarget.SELF;
  private static final CardType TYPE = CardType.POWER;
  public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

  private static final int COST = 2;

  private static final int DAMAGE = 6;

  private static final int AP_SCALING = 10;

  public ChaosStorm() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseDamage = DAMAGE;
    setAbilityPowerScaling(AP_SCALING);
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    if (p instanceof AbstractLeagueChar) {
      ((AbstractLeagueChar)p).playCardSound("ViktorChaosStorm", 0.05f);
  }
    AbstractDungeon.actionManager
        .addToBottom(new ApplyPowerAction(p, p, new ChaosStormPower(p, this.damage), this.damage));
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      this.isInnate = true;
      this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
      initializeDescription();
    }
  }
}
