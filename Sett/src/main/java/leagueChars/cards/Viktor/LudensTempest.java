package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LudensTempest extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(LudensTempest.class.getSimpleName());
    public static final String IMG = makeCardPath("LudensTempest.png");
    public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;
    private static final int AP_SCALING = 20;
    private static final int UPGRADE_AP_SCALING = 10;
    private static final int DAMAGE = 7;
    private static final int UPGRADE_PLUS_DMG = 3;

    public LudensTempest() {
        super(ID, IMG, 1, CardType.ATTACK, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseDamage = DAMAGE;
        this.isMultiDamage = true;
        setAbilityPowerScaling(AP_SCALING);

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        this.addToBot(new DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn,
                AbstractGameAction.AttackEffect.NONE));
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,5),5));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
            upgradeDamage(UPGRADE_PLUS_DMG);


        }

    }

}
