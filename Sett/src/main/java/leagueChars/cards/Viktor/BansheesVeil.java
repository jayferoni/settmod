package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.ArtifactPower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class BansheesVeil extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(BansheesVeil.class.getSimpleName());
    public static final String IMG = makeCardPath("BansheesVeil.png");
    public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

    public BansheesVeil() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseMagicNumber = 10;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));
        this.addToBot(new ApplyPowerAction(p, p, new ArtifactPower(p, 1), 1));




    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(10);



        }

    }

}
