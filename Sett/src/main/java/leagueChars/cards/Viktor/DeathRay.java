package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Viktor;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DeathRay extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(DeathRay.class.getSimpleName());
  public static final String IMG = makeCardPath("DeathRay.png");

  private static final CardRarity RARITY = CardRarity.BASIC;
  private static final CardTarget TARGET = CardTarget.ALL_ENEMY;
  private static final CardType TYPE = CardType.ATTACK;
  public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

  private static final int COST = 1;

  private static final int DAMAGE = 8;
  private static final int UPGRADE_PLUS_DMG = 3;
  private static final int AP_SCALING = 20;
  private static final int UPGRADE_AP_SCALING = 10;

  public DeathRay() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseDamage = DAMAGE;
    this.isMultiDamage = true;
    setAbilityPowerScaling(AP_SCALING);
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    if (p instanceof AbstractLeagueChar) {
      ((AbstractLeagueChar)p).playCardSound("ViktorDeathRay", 0.05f);
  }
    AbstractDungeon.actionManager
        .addToBottom(new DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn, AttackEffect.LIGHTNING));
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      upgradeDamage(UPGRADE_PLUS_DMG);
      upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
      initializeDescription();
    }
  }
}