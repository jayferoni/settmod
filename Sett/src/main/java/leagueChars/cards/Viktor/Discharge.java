package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Viktor;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Discharge extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(Discharge.class.getSimpleName());
  public static final String IMG = makeCardPath("Discharge.png");

  private static final CardRarity RARITY = CardRarity.SPECIAL;
  private static final CardTarget TARGET = CardTarget.ENEMY;
  private static final CardType TYPE = CardType.ATTACK;
  public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

  private static final int COST = 1;

  private static final int DAMAGE = 8;
  private static final int UPGRADE_PLUS_DMG = 3;
  private static final int AP_SCALING = 10;
  private static final int UPGRADE_AP_SCALING = 5;

  public Discharge() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseDamage = DAMAGE;
    this.exhaust = true;
    this.isEthereal = true;
    setAbilityPowerScaling(AP_SCALING);
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    if (p instanceof AbstractLeagueChar) {
      ((AbstractLeagueChar)p).playCardSound("Discharge", 0.05f);
  }
    AbstractDungeon.actionManager.addToBottom(
        new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn), AbstractGameAction.AttackEffect.LIGHTNING));
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      upgradeDamage(UPGRADE_PLUS_DMG);
      initializeDescription();
      upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
    }
  }
}
