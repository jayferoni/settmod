package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;
import leagueChars.powers.InfinitePowerPower;
import leagueChars.powers.QSpamPower;
import leagueChars.powers.RiftmakerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class InfinitePower extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * In-Progress Form At the start of your turn, play a TOUCH.
     */

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(InfinitePower.class.getSimpleName());
    public static final String IMG = makeCardPath("InfinitePower.png");

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.RARE;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.POWER;
    public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

    private static final int COST = 2;


    // /STAT DECLARATION/

    public InfinitePower() {

        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        this.baseMagicNumber = 10;
        this.magicNumber = this.baseMagicNumber;

    }

    // Actions the card should do.
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new InfinitePowerPower(p,this.magicNumber),this.magicNumber));

    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            this.upgradeMagicNumber(5);

            initializeDescription();

        }
    }
}
