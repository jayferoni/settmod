package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Viktor;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class NightHarvester extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(NightHarvester.class.getSimpleName());
    public static final String IMG = makeCardPath("NightHarvester.png");
    public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;
    private static final int AP_SCALING = 40;
    private static final int UPGRADE_AP_SCALING = 20;
    private static final int DAMAGE = 15;
    private static final int UPGRADE_PLUS_DMG = 5;

    public NightHarvester() {
        super(ID, IMG, 2, CardType.ATTACK, COLOR, CardRarity.RARE, CardTarget.ENEMY);
        this.baseDamage = DAMAGE;

        setAbilityPowerScaling(AP_SCALING);

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_VERTICAL));




    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
            upgradeDamage(UPGRADE_PLUS_DMG);


        }

    }

}
