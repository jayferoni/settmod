package leagueChars.cards.Viktor;

import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;

import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Viktor;

public class EmpoweredGravityField extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(EmpoweredGravityField.class.getSimpleName());
  public static final String IMG = LeagueCharsMod.makeCardPath("EmpoweredGravityField.png");

  private static final CardRarity RARITY = CardRarity.SPECIAL;
  private static final CardTarget TARGET = CardTarget.ALL_ENEMY;
  private static final CardType TYPE = CardType.SKILL;
  public static final CardColor COLOR = Viktor.Enums.COLOR_DARK_RED;

  private static final int COST = 2;

  private static final int MAGIC = 3;
  private static final int UPGRADE_PLUS_MAGIC = 2;

  public EmpoweredGravityField() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseMagicNumber = MAGIC;
    this.magicNumber = baseMagicNumber;
    this.exhaust = true;
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    if (p instanceof AbstractLeagueChar) {
      ((AbstractLeagueChar) p).playCardSound("ViktorGravityField", 0.05f);
    }
    for (AbstractMonster mo : AbstractDungeon.getCurrRoom().monsters.monsters) {
      this.addToBot(new ApplyPowerAction(mo, p, new WeakPower(mo, this.magicNumber, false), this.magicNumber, true,
          AttackEffect.NONE));
      this.addToBot(new ApplyPowerAction(mo, p, new VulnerablePower(mo, this.magicNumber, false), this.magicNumber,
          true, AttackEffect.NONE));
    }
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      upgradeMagicNumber(UPGRADE_PLUS_MAGIC);
      initializeDescription();
    }
  }
}
