package leagueChars.cards.sharedAD;
import leagueChars.cardPools.AttackDamage;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.StrengthPower;
import com.megacrit.cardcrawl.vfx.combat.InflameEffect;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LevelUp extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(LevelUp.class.getSimpleName());
    public static final String IMG = makeCardPath("LevelUp.png");
    public static final CardColor COLOR = AttackDamage.Enums.COLOR;

    public LevelUp() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new VFXAction(p, new InflameEffect(p), 1.0F));
        this.addToBot(new ApplyPowerAction(p, p, new StrengthPower(p, this.magicNumber), this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(0);
        }

    }

}
