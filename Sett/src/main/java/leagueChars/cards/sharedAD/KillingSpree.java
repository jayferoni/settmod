package leagueChars.cards.sharedAD;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import leagueChars.cardPools.AttackDamage;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.unique.LimitBreakAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class KillingSpree extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(KillingSpree.class.getSimpleName());
    public static final String IMG = makeCardPath("Killingspree.png");
    public static final CardColor COLOR = AttackDamage.Enums.COLOR;
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);


    public KillingSpree() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new LimitBreakAction());
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.exhaust = false;
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            initializeDescription();

        }

    }

}
