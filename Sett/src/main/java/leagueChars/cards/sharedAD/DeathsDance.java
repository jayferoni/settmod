package leagueChars.cards.sharedAD;

import leagueChars.cardPools.AttackDamage;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.status.Wound;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DeathsDance extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DeathsDance.class.getSimpleName());
    public static final String IMG = makeCardPath("DeathsDance.png");
    public static final CardColor COLOR = AttackDamage.Enums.COLOR;

    public DeathsDance() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseBlock = 13;
        this.cardsToPreview = new Wound();
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        AbstractDungeon.actionManager
                .addToBottom(new ApplyPowerAction(p, p, new RegenPower(p, this.magicNumber), this.magicNumber));
        this.addToBot(new MakeTempCardInHandAction(new Wound(), 2));
        this.addToBot(new GainBlockAction(p, p, this.block));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(3);
            this.upgradeMagicNumber(1);
        }

    }

}
