package leagueChars.cards.sharedAD;
import leagueChars.cardPools.AttackDamage;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.watcher.VigorPower;
import com.megacrit.cardcrawl.vfx.combat.FlameBarrierEffect;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class TrinityForce extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(TrinityForce.class.getSimpleName());
    public static final String IMG = makeCardPath("TrinityForce.png");
    public static final CardColor COLOR = AttackDamage.Enums.COLOR;

    public TrinityForce() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.baseMagicNumber = 8;
        this.magicNumber = this.baseMagicNumber;

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (Settings.FAST_MODE) {
            this.addToBot(new VFXAction(p, new FlameBarrierEffect(p.hb.cX, p.hb.cY), 0.1F));
        } else {
            this.addToBot(new VFXAction(p, new FlameBarrierEffect(p.hb.cX, p.hb.cY), 0.5F));
        }

        this.addToBot(new ApplyPowerAction(p, p, new VigorPower(p, this.magicNumber), this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(2);
        }

    }

}
