package leagueChars.cards.Mordekaiser;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.MetallicizePower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DemonicEmbrace extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DemonicEmbrace.class.getSimpleName());
    public static final String IMG = makeCardPath("DemonicEmbrace.png");
    public static final CardColor COLOR = Mordekaiser.Enums.COLOR_GRAY;

    public DemonicEmbrace() {
        super(ID, IMG, 2, CardType.POWER, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseMagicNumber = 10;
        this.magicNumber = this.baseMagicNumber;

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));
        this.addToBot(new ApplyPowerAction(p, p, new MetallicizePower(p, 5), 5));




    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(5);





        }

    }

}
