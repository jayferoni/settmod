package leagueChars.cards.Mordekaiser;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;
import leagueChars.powers.AbilityPowerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class CosmicDrive extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(CosmicDrive.class.getSimpleName());
    public static final String IMG = makeCardPath("CosmicDrive.png");
    public static final CardColor COLOR = Mordekaiser.Enums.COLOR_GRAY;

    public CosmicDrive() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.SELF);
        this.baseMagicNumber = 5;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p, this.magicNumber), this.magicNumber));
        this.addToBot(new DrawCardAction(p, 1));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(5);
        }
    }

}
