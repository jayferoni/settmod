package leagueChars.cards.Mordekaiser;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.vfx.combat.CleaveEffect;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class APAttack extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(APAttack.class.getSimpleName());
  public static final String IMG = makeCardPath("APAttack.png");

  private static final CardRarity RARITY = CardRarity.COMMON;
  private static final CardTarget TARGET = CardTarget.ENEMY;
  private static final CardType TYPE = CardType.ATTACK;
  public static final CardColor COLOR = Mordekaiser.Enums.COLOR_GRAY;

  private static final int COST = 1;

  private static final int DAMAGE = 5;
  private static final int UPGRADE_PLUS_DMG = 2;
  private static final int AP_SCALING = 15;
  private static final int UPGRADE_AP_SCALING = 5;

  public APAttack() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    this.baseDamage = DAMAGE;

    setAbilityPowerScaling(AP_SCALING);
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {

    AbstractDungeon.actionManager.addToBottom(
        new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn), AbstractGameAction.AttackEffect.NONE));
    this.addToBot(new VFXAction(p, new CleaveEffect(), 0.1F));
  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();
      upgradeDamage(UPGRADE_PLUS_DMG);
      initializeDescription();
      upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
    }
  }
}
