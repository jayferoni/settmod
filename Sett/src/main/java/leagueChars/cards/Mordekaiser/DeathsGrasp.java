package leagueChars.cards.Mordekaiser;

import static leagueChars.LeagueCharsMod.makeCardPath;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Mordekaiser;
import leagueChars.powers.DeathRayAfterBurnPower;
import leagueChars.powers.DeathsGraspPower;

public class DeathsGrasp extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */


    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(DeathsGrasp.class.getSimpleName());
    public static final String IMG = makeCardPath("DeathsGrasp.png");

    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.ALL_ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Mordekaiser.Enums.COLOR_GRAY;

    private static final int COST = 0;

    private static final int DAMAGE = 4;
    private static final int UPGRADE_PLUS_DMG = 2;
    private static final int AP_SCALING = 5;
    private static final int UPGRADE_AP_SCALING = 5;


    // /STAT DECLARATION/


    public DeathsGrasp() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.isMultiDamage=true;

        this.magicNumber = this.baseMagicNumber;
        setAbilityPowerScaling(AP_SCALING);

    }


    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar)p).playCardSound("MordeE");
        }

        this.addToBot(new ApplyPowerAction(p, p, new DeathsGraspPower(p, 1), 1));
        this.addToBot(new ApplyPowerAction(p, p, new DeathRayAfterBurnPower(p, this.damage), this.damage));
    }

    //Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(UPGRADE_PLUS_DMG);

            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
        }
    }
}