package leagueChars.cards.Mordekaiser;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;
import leagueChars.powers.AbilityPowerPower;
import leagueChars.powers.RiftmakerPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Riftmaker extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(Riftmaker.class.getSimpleName());
    public static final String IMG = makeCardPath("Riftmaker.png");
    public static final CardColor COLOR = Mordekaiser.Enums.COLOR_GRAY;

    public Riftmaker() {
        super(ID, IMG, 2, CardType.POWER, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.baseMagicNumber = 10;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new AbilityPowerPower(p,this.magicNumber),this.magicNumber));
        this.addToBot(new ApplyPowerAction(p, p, new RiftmakerPower(p,1),1));





    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(10);



        }

    }

}
