package leagueChars.cards.Mordekaiser;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.MonsterGroup;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.vfx.combat.CleaveEffect;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Mordekaiser;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Obliterate extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(Obliterate.class.getSimpleName());
    public static final String IMG = makeCardPath("Obliterate.png");

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.ALL_ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Mordekaiser.Enums.COLOR_GRAY;

    private static final int COST = 1;

    private static final int DAMAGE = 7;
    private static final int UPGRADE_PLUS_DMG = 3;
    private static final int AP_SCALING = 15;
    private static final int UPGRADE_AP_SCALING = 5;

    // /STAT DECLARATION/

    public Obliterate() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.isMultiDamage = true;
        setAbilityPowerScaling(AP_SCALING);
    }

    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        List<AbstractMonster> monsters = AbstractDungeon.getMonsters().monsters.stream()
                .filter(abstractMonster -> !abstractMonster.isDead).collect(Collectors.toList());
        if (monsters.size() == 1) {
            this.addToBot(new DamageAllEnemiesAction(p, Arrays.stream(this.multiDamage).map(i -> i * 2).toArray(),
                    this.damageTypeForTurn, AbstractGameAction.AttackEffect.NONE));

        } else {
            this.addToBot(new DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn,
                    AbstractGameAction.AttackEffect.NONE));
        }
        this.addToBot(new SFXAction("MordeQ"));
        this.addToBot(new VFXAction(p, new CleaveEffect(), 0.1F));

    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(UPGRADE_PLUS_DMG);
            upgradeAbilityPowerScaling(UPGRADE_AP_SCALING);
        }
    }
}