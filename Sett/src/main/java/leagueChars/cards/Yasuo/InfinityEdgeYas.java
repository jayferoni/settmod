package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.cards.deprecated.DEPRECATEDHotHot;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Tryndamere;
import leagueChars.characters.Yasuo;
import leagueChars.powers.CriticalStrikeChancePower;
import leagueChars.powers.InfinityEdgePower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class InfinityEdgeYas extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */
    // WORK IN PROGRESS

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(InfinityEdgeYas.class.getSimpleName());
    public static final String IMG = makeCardPath("InfinityEdge.png");
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);


    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.RARE;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.POWER;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 1;
    private static final int UPGRADE_COST = 1;


    // /STAT DECLARATION/

    public InfinityEdgeYas() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    }

    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new CriticalStrikeChancePower(p, 15), 15));
        this.addToBot(new ApplyPowerAction(p, p, new InfinityEdgePower(p)));
    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADE_COST);
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            this.isInnate=true;
        }
    }
}