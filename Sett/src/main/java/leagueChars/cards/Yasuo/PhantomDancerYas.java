package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Tryndamere;
import leagueChars.characters.Yasuo;
import leagueChars.powers.CriticalStrikeChancePower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class PhantomDancerYas extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */


    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(PhantomDancerYas.class.getSimpleName());
    public static final String IMG = makeCardPath("PhantomDancer.png");

    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.COMMON;
    private static final CardTarget TARGET = CardTarget.ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 1;

    private static final int DAMAGE = 6;


    // /STAT DECLARATION/


    public PhantomDancerYas() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;

    }


    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
        this.addToBot(new DrawCardAction(p, this.magicNumber));
        this.addToBot(new ApplyPowerAction(p, p, new CriticalStrikeChancePower(p,5),5));

    }


    //Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            this.upgradeDamage(1);
            this.upgradeMagicNumber(1);

        }
    }
}