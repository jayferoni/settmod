package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.RemoveAllBlockAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.MonsterGroup;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.SettTheBoss;
import leagueChars.characters.Yasuo;
import leagueChars.powers.CriticalStrikeChancePower;
import leagueChars.powers.LastBreathPower;
import leagueChars.powers.SweepingBladePower;
import leagueChars.vfx.HaymakerEffect;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LastBreath extends AbstractDynamicCard {

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(LastBreath.class.getSimpleName());
    public static final String IMG = makeCardPath("LastBreath.png");

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.ALL_ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);


    private static final int COST = 0;
    private static final int DAMAGE = 15;
    private static final int UPGRADE_PLUS_DMG = 5;

    // /STAT DECLARATION/

    public LastBreath() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.isInnate =true;



    }

    // Actions the card should do.
    @Override
    public void use(AbstractPlayer p, AbstractMonster _m) {
        MonsterGroup monsters = AbstractDungeon.getMonsters();
        monsters.monsters.stream().filter(m -> m.getPower(LastBreathPower.POWER_ID) != null && !(m.isDead || m.isDying)).forEach(m -> {
            this.addToBot(new RemoveAllBlockAction(m, p));
            this.addToBot(new ApplyPowerAction(p, p, new CriticalStrikeChancePower(p,15),15));
            this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.NONE));
            this.addToBot(new RemoveSpecificPowerAction(m, p, LastBreathPower.POWER_ID));



        });

        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar) p).playCardSound("YasuoR", 0.05f);
        }

    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(UPGRADE_PLUS_DMG);
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            initializeDescription();
            this.selfRetain = true;


        }
    }
}