package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.Nexus;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.cards.Viktor.Discharge;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Yasuo;
import leagueChars.powers.SweepingBladePower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class SteelTempest1 extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(SteelTempest1.class.getSimpleName());
    public static final String IMG = makeCardPath("SteelTempest.png");

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 1;

    private static final int DAMAGE = 6;
    private static final int UPGRADE_PLUS_DMG = 2;

    // /STAT DECLARATION/

    public SteelTempest1() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.exhaust = true;

    }

    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        SweepingBladePower sweepingBladePower = (SweepingBladePower)m.getPower(SweepingBladePower.POWER_ID);
        if (sweepingBladePower == null) {
            this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
        } else {
            this.addToBot(new DamageAllEnemiesAction(p, this.damage, this.damageTypeForTurn, AttackEffect.SLASH_HEAVY));
            this.addToBot(new RemoveSpecificPowerAction(m, p, SweepingBladePower.POWER_ID));


        }
        SteelTempest2 steelTempest2 = new SteelTempest2();
        if (this.upgraded) {
            steelTempest2.upgrade();
        }
        this.addToBot(new MakeTempCardInHandAction(steelTempest2));
        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar) p).playCardSound("YasuoQ1", 0.05f);
        }
    }


    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(UPGRADE_PLUS_DMG);
        }
    }
}