package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Tryndamere;
import leagueChars.characters.Yasuo;
import leagueChars.powers.CriticalStrikeChancePower;

public class CloakofAgilityYas extends AbstractDynamicCard {

    public static final String ID = LeagueCharsMod.makeID(CloakofAgilityYas.class.getSimpleName());
    public static final String IMG = LeagueCharsMod.makeCardPath("CloakofAgility.png");// "public static final String IMG = makeCardPath("${NAME}.png");
    // This does mean that you will need to have an image with the same NAME as the card in your image folder for it to run correctly.


    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.COMMON; //  Up to you, I like auto-complete on these
    private static final CardTarget TARGET = CardTarget.SELF;  //   since they don't change much.
    private static final CardType TYPE = CardType.SKILL;       //
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 1;  // COST = ${COST}

    private static final int DAMAGE = 0;    // DAMAGE = ${DAMAGE}
    private static final int UPGRADE_PLUS_DMG = 0;  // UPGRADE_PLUS_DMG = ${UPGRADED_DAMAGE_INCREASE}

    // /STAT DECLARATION/


    public CloakofAgilityYas() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
  this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new CriticalStrikeChancePower(p,10),10));

    }
    @Override
    public void upgrade() {
        this.upgradeName();
        this.upgradeDamage(UPGRADE_PLUS_DMG);
        this.upgradeBaseCost(0);

    }
}
