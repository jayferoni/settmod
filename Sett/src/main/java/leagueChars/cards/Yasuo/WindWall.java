package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Viktor;
import leagueChars.characters.Yasuo;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class WindWall extends AbstractDynamicCard {

  public static final String ID = LeagueCharsMod.makeID(WindWall.class.getSimpleName());
  public static final String IMG = makeCardPath("WindWall.png");

  private static final CardRarity RARITY = CardRarity.BASIC;
  private static final CardTarget TARGET = CardTarget.SELF;
  private static final CardType TYPE = CardType.SKILL;
  public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;


  private static final int COST = 2;

  private static final int BLOCK = 30;
  private static final int UPGRADE_PLUS_BLOCK = 20;

  public WindWall() {
    super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);

    this.baseBlock = BLOCK;
    this.cardsToPreview = new FailedWindWall();
    this.exhaust = true;
  }

  @Override
  public void use(AbstractPlayer p, AbstractMonster m) {
    this.addToBot(new GainBlockAction(p, this.block));

    FailedWindWall failedWindWall = new FailedWindWall();
    if (this.upgraded) {
      failedWindWall.upgrade();
    }
    this.addToBot(new MakeTempCardInHandAction(failedWindWall));
    if (p instanceof AbstractLeagueChar) {
      ((AbstractLeagueChar) p).playCardSound("YasuoW", 0.05f);
    }  }

  @Override
  public void upgrade() {
    if (!upgraded) {
      upgradeName();

      upgradeBlock(UPGRADE_PLUS_BLOCK);
      initializeDescription();
    }
  }
}
