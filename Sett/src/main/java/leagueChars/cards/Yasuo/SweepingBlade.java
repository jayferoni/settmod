package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.Nexus;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Yasuo;
import leagueChars.powers.CriticalStrikeChancePower;
import leagueChars.powers.SweepingBladePower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import static leagueChars.LeagueCharsMod.makeCardPath;

public class SweepingBlade extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(SweepingBlade.class.getSimpleName());
    public static final String IMG = makeCardPath("SweepingBlade.png");
    private static final Logger logger = LogManager.getLogger(SweepingBlade.class.getName());




    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);


    private static final int COST = 0;

    private static final int DAMAGE = 1;
    private static final int UPGRADE_PLUS_DMG = 1;

    // /STAT DECLARATION/

    public SweepingBlade() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;

    }

    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {

        if (p instanceof AbstractLeagueChar) {
            this.addToBot(new ApplyPowerAction(p, p, new CriticalStrikeChancePower(p,5),5));
            ((AbstractLeagueChar) p).playCardSound("YasuoE", 0.05f);
        }        this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
        this.addToBot(new ApplyPowerAction(m, p, new SweepingBladePower(m, this.magicNumber), this.magicNumber, true, AbstractGameAction.AttackEffect.NONE));

    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(UPGRADE_PLUS_DMG);
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            initializeDescription();
            this.selfRetain = true;
        }
    }
}