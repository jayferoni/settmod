package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Tryndamere;
import leagueChars.characters.Yasuo;
import leagueChars.powers.BerserkersGreavesPower;
import leagueChars.powers.CriticalStrikeChancePower;
import leagueChars.powers.NavorisQuickbladePower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class BerserkersGreaves extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */
    // WORK IN PROGRESS

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(BerserkersGreaves.class.getSimpleName());
    public static final String IMG = makeCardPath("BerserkerGreaves.png");
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.RARE;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.POWER;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 1;

    private static final int DAMAGE = 0;

    // /STAT DECLARATION/

    public BerserkersGreaves() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;


    }

    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
                this.addToBot(new ApplyPowerAction(p, p, new BerserkersGreavesPower(p, 1), 1));
    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            this.isInnate = true;
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            initializeDescription();
        }
    }
}