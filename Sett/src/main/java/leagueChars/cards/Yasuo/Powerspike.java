package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.StrengthPower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Yasuo;
import leagueChars.powers.CriticalStrikeChancePower;

public class Powerspike extends AbstractDynamicCard {

    public static final String ID = LeagueCharsMod.makeID(Powerspike.class.getSimpleName());
    public static final String IMG = LeagueCharsMod.makeCardPath("Powerspike.png");// "public static final String IMG = makeCardPath("${NAME}.png");
    // This does mean that you will need to have an image with the same NAME as the card in your image folder for it to run correctly.


    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.RARE; //  Up to you, I like auto-complete on these
    private static final CardTarget TARGET = CardTarget.SELF;  //   since they don't change much.
    private static final CardType TYPE = CardType.SKILL;       //
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 3;  // COST = ${COST}

    private static final int DAMAGE = 0;    // DAMAGE = ${DAMAGE}
    private static final int UPGRADE_PLUS_DMG = 0;  // UPGRADE_PLUS_DMG = ${UPGRADED_DAMAGE_INCREASE}

    // /STAT DECLARATION/


    public Powerspike() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
  this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new CriticalStrikeChancePower(p,50),50));
        this.addToBot(new ApplyPowerAction(p, p, new StrengthPower(p, this.magicNumber), this.magicNumber));

    }
    @Override
    public void upgrade() {
        this.upgradeName();
        this.upgradeDamage(UPGRADE_PLUS_DMG);
        this.upgradeBaseCost(2);

    }
}
