package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import basemod.abstracts.cardbuilder.actionbuilder.ApplyPowerActionBuilder;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.Nexus;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Yasuo;
import leagueChars.powers.LastBreathPower;
import leagueChars.powers.SweepingBladePower;

import static leagueChars.LeagueCharsMod.makeCardPath;

import javax.swing.plaf.nimbus.AbstractRegionPainter;

public class SteelTempest3 extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(SteelTempest3.class.getSimpleName());
    public static final String IMG = makeCardPath("SteelTempestFinisher.png");

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.SPECIAL;
    private static final CardTarget TARGET = CardTarget.ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    private static final int COST = 1;

    private static final int DAMAGE = 10;
    private static final int UPGRADE_PLUS_DMG = 2;

    // /STAT DECLARATION/

    public SteelTempest3() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.exhaust = true;

    }

    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        SweepingBladePower sweepingBladePower = (SweepingBladePower)m.getPower(SweepingBladePower.POWER_ID);
        if (sweepingBladePower == null) {
            this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
            this.addToBot(new ApplyPowerAction(m, p, new LastBreathPower(m)));

        } else {
            this.addToBot(new DamageAllEnemiesAction(p, this.damage, this.damageTypeForTurn, AttackEffect.SLASH_HEAVY));
            AbstractDungeon.getMonsters().monsters.forEach(monster -> {
                this.addToBot(new ApplyPowerAction(monster, p, new LastBreathPower(m)));
                this.addToBot(new RemoveSpecificPowerAction(m, p, SweepingBladePower.POWER_ID));
            });
        }
        SteelTempest1 steelTempest1 = new SteelTempest1();
        if (this.upgraded) {
            steelTempest1.upgrade();

        }


        this.addToBot(new MakeTempCardInHandAction(steelTempest1));
        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar) p).playCardSound("YasuoQ3", 0.05f);
        }    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(UPGRADE_PLUS_DMG);
        }
    }
}