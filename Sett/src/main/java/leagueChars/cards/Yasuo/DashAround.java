package leagueChars.cards.Yasuo;

import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cardPools.AttackDamage;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.cards.shared.DoransBlade;
import leagueChars.characters.Yasuo;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DashAround extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DashAround.class.getSimpleName());
    public static final String IMG = makeCardPath("Dash.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);
    public static final CardColor COLOR = Yasuo.Enums.COLOR_DARK_RED;

    public DashAround() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        this.addToBot(new MakeTempCardInHandAction(new SweepingBlade(), 3));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(0);

        }

    }

}
