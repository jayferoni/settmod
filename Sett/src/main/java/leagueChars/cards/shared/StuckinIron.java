package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import basemod.helpers.BaseModCardTags;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import leagueChars.powers.DoransBPower;
import leagueChars.powers.DoransRPower;
import leagueChars.powers.DoransSPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class StuckinIron extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * In-Progress Form At the start of your turn, play a TOUCH.
     */

    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(StuckinIron.class.getSimpleName());
    public static final String IMG = makeCardPath("StuckinIron.png");

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.UNCOMMON;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.POWER;
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    private static final int COST = 1;
    private static final int UPGRADE_COST = 1;

    private static final int MAGIC = 2;

    // /STAT DECLARATION/

    public StuckinIron() {

        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        magicNumber = baseMagicNumber = MAGIC;

        this.tags.add(BaseModCardTags.FORM); // Tag your strike, defend and form cards so that they work correctly.
        this.magicNumber = this.baseMagicNumber;
        this.cardsToPreview = new DoransBlade();
    }

    // Actions the card should do.
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new DoransBPower(p, this.magicNumber), this.magicNumber));
        this.addToBot(new ApplyPowerAction(p, p, new DoransSPower(p, this.magicNumber), this.magicNumber));
        this.addToBot(new ApplyPowerAction(p, p, new DoransRPower(p, this.magicNumber), this.magicNumber));
    }

    // Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADE_COST);
            initializeDescription();
            this.upgradeMagicNumber(2);
        }
    }
}
