package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Ghost extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(Ghost.class.getSimpleName());
    public static final String IMG = makeCardPath("Ghost.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public Ghost() {
        super(ID, IMG, 0, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.NONE);
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        {
            this.addToBot(new DrawCardAction(p, this.magicNumber));
        }
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(1);
        }

    }

}
