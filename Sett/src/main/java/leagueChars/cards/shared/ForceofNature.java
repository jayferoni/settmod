package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class ForceofNature extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(ForceofNature.class.getSimpleName());
    public static final String IMG = makeCardPath("ForceofNature.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public ForceofNature() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.SELF);
        this.baseBlock = 8;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new GainBlockAction(p, p, this.block));
        this.addToBot(new DrawCardAction(p, 1));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(3);
        }

    }

}
