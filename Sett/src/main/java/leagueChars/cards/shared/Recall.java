package leagueChars.cards.shared;

import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.EnergizedPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Recall extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(Recall.class.getSimpleName());
    public static final String IMG = makeCardPath("Recall.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);

    public Recall() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.NONE);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (!this.upgraded) {
            this.addToBot(new ApplyPowerAction(p, p, new EnergizedPower(p, 2), 2));
        } else {
            this.addToBot(new ApplyPowerAction(p, p, new EnergizedPower(p, 3), 3));
        }

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            initializeDescription();
        }

    }

}
