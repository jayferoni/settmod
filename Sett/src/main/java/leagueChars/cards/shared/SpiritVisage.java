package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class SpiritVisage extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(SpiritVisage.class.getSimpleName());
    public static final String IMG = makeCardPath("SpiritVisage.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public SpiritVisage() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.baseBlock = 12;
        this.baseMagicNumber = 3;
        this.exhaust = true;

        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        this.addToBot(new GainBlockAction(p, p, this.block));
        AbstractDungeon.actionManager
                .addToBottom(new ApplyPowerAction(p, p, new RegenPower(p, this.magicNumber), this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(2);
            this.upgradeMagicNumber(2);
        }

    }

}
