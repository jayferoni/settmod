package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.unique.FeedAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;
import leagueChars.LeagueCharsMod;
import leagueChars.actions.WarmogsAction;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Warmogs extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(Warmogs.class.getSimpleName());
    public static final String IMG = makeCardPath("Warmogs.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public Warmogs() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);

        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player,
                new RegenPower(AbstractDungeon.player, this.magicNumber), this.magicNumber));
        this.addToBot(new WarmogsAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), this.magicNumber));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            this.upgradeMagicNumber(1);
        }

    }

}
