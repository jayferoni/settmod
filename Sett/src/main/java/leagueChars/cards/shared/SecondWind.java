package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class SecondWind extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(SecondWind.class.getSimpleName());
    public static final String IMG = makeCardPath("SecondWind.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public SecondWind() {
        super(ID,IMG,1,CardType.SKILL, COLOR,CardRarity.UNCOMMON,CardTarget.SELF);
        this.baseMagicNumber =2;
        this.exhaust=true;
        this.magicNumber = this.baseMagicNumber;
    }
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(p, p, new RegenPower(p, this.magicNumber), this.magicNumber));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(1);
        }

    }



}
