package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.watcher.FreeAttackPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DeadmansPlate extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DeadmansPlate.class.getSimpleName());
    public static final String IMG = makeCardPath("DeadmansPlate.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public DeadmansPlate() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseBlock = 8;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        this.addToBot(new GainBlockAction(p, p, this.block));
        this.addToBot(new ApplyPowerAction(p, p, new FreeAttackPower(p, 1), 1));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(3);
        }

    }

}
