package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Flash extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(Flash.class.getSimpleName());
    public static final String IMG = makeCardPath("Flash.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public Flash() {
        super(ID, IMG, 0, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.NONE);
        this.exhaust = true;
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new GainEnergyAction(this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(1);
        }

    }

}
