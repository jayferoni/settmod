package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.PhantasmalPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class JunglerGank extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(JunglerGank.class.getSimpleName());
    public static final String IMG = makeCardPath("JunglerGank.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public JunglerGank() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new PhantasmalPower(p, 1), 1));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(0);
        }

    }

}
