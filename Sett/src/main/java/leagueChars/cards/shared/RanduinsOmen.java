package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.WeakPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class RanduinsOmen extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(RanduinsOmen.class.getSimpleName());
    public static final String IMG = makeCardPath("RanduinsOmen.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public RanduinsOmen() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.ENEMY);
        this.baseBlock = 11;
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(m, p, new WeakPower(m, this.magicNumber, false), this.magicNumber));
        this.addToBot(new GainBlockAction(p, p, this.block));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(3);
            this.upgradeMagicNumber(1);
        }

    }

}
