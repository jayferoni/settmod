package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.unique.DoubleYourBlockAction;
import com.megacrit.cardcrawl.actions.utility.ExhaustAllEtherealAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class GargoylesStoneplate extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(GargoylesStoneplate.class.getSimpleName());
    public static final String IMG = makeCardPath("GargoylesStoneplate.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public GargoylesStoneplate() {
        super(ID, IMG, 2, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        {
            this.addToBot(new DoubleYourBlockAction(p));

        }

    }

    public void triggerOnEndOfPlayerTurn() {
        this.addToTop(new ExhaustAllEtherealAction());
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(1);
        }

    }

}
