package leagueChars.cards.shared;

import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import com.megacrit.cardcrawl.actions.unique.ExpertiseAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class BootsofSwiftness extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(BootsofSwiftness.class.getSimpleName());
    public static final String IMG = makeCardPath("BootsofSwiftness.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public BootsofSwiftness() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseMagicNumber = 6;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ExpertiseAction(p, this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(1);
        }

    }

}
