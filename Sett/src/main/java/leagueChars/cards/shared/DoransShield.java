package leagueChars.cards.shared;
import com.megacrit.cardcrawl.actions.common.HealAction;
import leagueChars.cardPools.Nexus;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class DoransShield extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(DoransShield.class.getSimpleName());
    public static final String IMG = makeCardPath("DoransShield.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public DoransShield() {
        super(ID, IMG, 0, CardType.SKILL, COLOR, CardRarity.COMMON, CardTarget.SELF);
        this.baseBlock = 3;
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        AbstractDungeon.actionManager
                .addToBottom(new HealAction(p, p,  this.magicNumber));
        this.addToBot(new GainBlockAction(p, p, this.block));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(2);

        }

    }

}
