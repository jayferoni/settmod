package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.status.Wound;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class ChainVest extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(ChainVest.class.getSimpleName());
    public static final String IMG = makeCardPath("ChainVest.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public ChainVest() {
        super(ID, IMG, 1, CardType.SKILL, COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseBlock = 10;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (Settings.isDebug) {
            this.addToBot(new GainBlockAction(p, p, 50));
        } else {
            this.addToBot(new GainBlockAction(p, p, this.block));
            this.addToBot(new MakeTempCardInHandAction(new Wound(), 1));
        }

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(2);
        }

    }

}
