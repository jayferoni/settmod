package leagueChars.cards.shared;
import leagueChars.cardPools.Nexus;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.HealAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Heal extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(Heal.class.getSimpleName());
    public static final String IMG = makeCardPath("Heal.png");
    public static final CardColor COLOR = Nexus.Enums.COLOR;

    public Heal() {
        super(ID, IMG, 0, CardType.SKILL, COLOR, CardRarity.RARE, CardTarget.SELF);
        this.baseMagicNumber = 8;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;
        this.tags.add(CardTags.HEALING);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new HealAction(p, p, this.magicNumber));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(4);
        }

    }

}
