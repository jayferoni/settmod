package leagueChars.cards.tryn;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.MonsterGroup;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;

import basemod.abstracts.CustomCard;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Tryndamere;
import leagueChars.powers.BloodlustPower;
import leagueChars.relics.BattleFury;

public class MockingShout extends AbstractDynamicCard {

    public static final String ID = LeagueCharsMod.makeID("MockingShout");
    public static final String IMG = LeagueCharsMod.makeCardPath("MockingShout.png");// "public static final String IMG = makeCardPath("${NAME}.png");
    // This does mean that you will need to have an image with the same NAME as the card in your image folder for it to run correctly.


    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC; //  Up to you, I like auto-complete on these
    private static final CardTarget TARGET = CardTarget.ALL_ENEMY;  //   since they don't change much.
    private static final CardType TYPE = CardType.SKILL;       //
    public static final CardColor COLOR = Tryndamere.Enums.COLOR;

    private static final int COST = 1;  // COST = ${COST}
    private static final int UPGRADED_COST = 0; // UPGRADED_COST = ${UPGRADED_COST}
    private static final int DEBUFF_AMOUNT = 2;
    
    // /STAT DECLARATION/


    public MockingShout() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        this.baseMagicNumber = DEBUFF_AMOUNT;

        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar)p).playCardSound("TryndamereW", 0.05f);
        }
        MonsterGroup monsters = AbstractDungeon.getMonsters();
        for (AbstractMonster monster : monsters.monsters) {
            this.addToBot(new ApplyPowerAction(monster, p, new WeakPower(monster, this.magicNumber, false),
                    this.magicNumber, true, AbstractGameAction.AttackEffect.NONE));
            if (monster.getIntentBaseDmg() <= 0) {
                this.addToBot(new ApplyPowerAction(monster, p, new VulnerablePower(monster, this.magicNumber, false),
                        this.magicNumber, true, AbstractGameAction.AttackEffect.NONE));
            }
        }
    }
    @Override
    public void upgrade() {
        this.upgradeName();
        this.upgradeBaseCost(UPGRADED_COST);
    }
}
