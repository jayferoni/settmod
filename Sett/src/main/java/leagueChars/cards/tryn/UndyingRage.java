package leagueChars.cards.tryn;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.utility.SFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.Tryndamere;
import leagueChars.powers.UndyingRagePower;

public class UndyingRage extends AbstractDynamicCard {

    public static final String ID = LeagueCharsMod.makeID(UndyingRage.class.getSimpleName());
    public static final String IMG = LeagueCharsMod.makeCardPath("UndyingRage.png");
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);


    private static final CardRarity RARITY = CardRarity.RARE;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.SKILL;
    public static final CardColor COLOR = Tryndamere.Enums.COLOR;

    private static final int COST = 1;

    public UndyingRage() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        this.exhaust=true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar)p).playCardSound("TryndamereR", 0.05f);
        }
        this.addToBot(new ApplyPowerAction(p, p, new UndyingRagePower(p)));
    }

    @Override
    public void upgrade() {
        this.upgradeName();
        this.selfRetain = true;
        this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
        initializeDescription();
    }
}
