package leagueChars.cards.tryn;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DemonFormPower;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.cards.Mordekaiser.EmpObliterate;
import leagueChars.characters.SettTheBoss;
import leagueChars.characters.Tryndamere;
import leagueChars.powers.RunemDownPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class RunemDown extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(RunemDown.class.getSimpleName());
    public static final String IMG = makeCardPath("RunemDown.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public RunemDown() {
        super(ID, IMG, 2, CardType.POWER, Tryndamere.Enums.COLOR, CardRarity.UNCOMMON, CardTarget.SELF);
        this.exhaust = true;
        this.cardsToPreview = new FollowUp();
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new ApplyPowerAction(p, p, new RunemDownPower(p, this.magicNumber), this.magicNumber));

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();

            this.upgradeMagicNumber(1);

        }

    }



}
