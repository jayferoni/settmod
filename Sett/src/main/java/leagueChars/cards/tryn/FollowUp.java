package leagueChars.cards.tryn;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Tryndamere;
import leagueChars.powers.CriticalStrikeChancePower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class FollowUp extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */


    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(FollowUp.class.getSimpleName());
    public static final String IMG = makeCardPath("FollowUp.png");

    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.SPECIAL;
    private static final CardTarget TARGET = CardTarget.ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = Tryndamere.Enums.COLOR;

    private static final int COST = 0;

    private static final int DAMAGE = 4;


    // /STAT DECLARATION/


    public FollowUp() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.exhaust=true;
        this.isEthereal=true;


    }


    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));



    }


    //Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            this.upgradeDamage(1);


        }
    }
}