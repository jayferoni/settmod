package leagueChars.cards.tryn;

import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.cards.deprecated.DEPRECATEDHotHot;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.Tryndamere;
import leagueChars.powers.BloodlustPower;
import leagueChars.relics.BattleFury;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class Bloodlust extends AbstractDynamicCard {

    public static final String ID = LeagueCharsMod.makeID("Bloodlust");
    public static final String IMG = makeCardPath("Bloodlust.png");// "public static final String IMG =
                                                                   // makeCardPath("${NAME}.png");
    // This does mean that you will need to have an image with the same NAME as the
    // card in your image folder for it to run correctly.

    // /TEXT DECLARATION/

    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC; // Up to you, I like auto-complete on these
    private static final CardTarget TARGET = CardTarget.SELF; // since they don't change much.
    private static final CardType TYPE = CardType.SKILL; //
    public static final CardColor COLOR = Tryndamere.Enums.COLOR;

    private static final int COST = 0; // COST = ${COST}
    private static final int UPGRADED_COST = 0; // UPGRADED_COST = ${UPGRADED_COST}

    private static final int BASE_HEAL = 1;
    private static final double HEAL_PER_FURY = 0.1;
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);


    private int heal;

    // /STAT DECLARATION/

    public Bloodlust() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        this.heal = BASE_HEAL;
        this.magicNumber = this.baseMagicNumber = 0;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        Tryndamere tryn = (Tryndamere) p;
        BloodlustPower bloodlustP = tryn.getBloodlustPower();
        BattleFury bf = tryn.getBattleFuryRelic();
        int fury = bf.getFury();
        bf.resetFury();
        int healAmount = (int) (fury * HEAL_PER_FURY) + this.heal;
        this.addToBot(new HealAction(p, p, healAmount));
        bloodlustP.recalculateStrength();
    }

    @Override
    public void atTurnStart() {
        Tryndamere tryn = (Tryndamere) AbstractDungeon.player;
        BattleFury bf = tryn.getBattleFuryRelic();
        int fury = bf.getFury();
        int healAmount = (int) (fury * HEAL_PER_FURY) + this.heal;
        this.magicNumber = this.baseMagicNumber = healAmount;
    }

    @Override
    public void upgrade() {
        this.upgradeName();
        this.heal += 1;
        this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
        initializeDescription();
    }
}
