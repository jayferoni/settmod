package leagueChars.cards.Sett;

import leagueChars.LeagueCharsMod;
import leagueChars.actions.Combo;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.AbstractLeagueChar;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static com.megacrit.cardcrawl.core.CardCrawlGame.languagePack;
import static leagueChars.LeagueCharsMod.makeCardPath;

public class KnuckleDown extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * TOUCH Deal 30(35) damage.
     */


    // TEXT DECLARATION

    public static final String ID = LeagueCharsMod.makeID(KnuckleDown.class.getSimpleName());
    public static final String IMG = makeCardPath("KnuckleDown.png");

    // /TEXT DECLARATION/


    // STAT DECLARATION

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.ENEMY;
    private static final CardType TYPE = CardType.ATTACK;
    public static final CardColor COLOR = SettTheBoss.Enums.COLOR_GRAY;

    private static final int COST = 1;

    private static final int DAMAGE = 4;


    // /STAT DECLARATION/
    public KnuckleDown() {
        this(0);
    }

    public KnuckleDown(int upgrades) {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        baseDamage = DAMAGE;
        this.timesUpgraded = upgrades;
    }


    // Actions the card should do.
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (p instanceof AbstractLeagueChar) {
            ((AbstractLeagueChar)p).playCardSound("KnuckleDownSound");
        }
        this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
        this.addToBot(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_VERTICAL));
        this.addToBot(new Combo(m, new DamageInfo(p, this.damage, this.damageTypeForTurn)));
    }

    //Upgraded stats.
    @Override
    public void upgrade() {
        this.upgradeDamage(1 );
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = languagePack.getCardStrings(ID).NAME + "+" + this.timesUpgraded;
        this.initializeTitle();
        }
    public boolean canUpgrade() {
        return true;
    }
    }
