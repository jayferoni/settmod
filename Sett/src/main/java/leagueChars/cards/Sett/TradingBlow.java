package leagueChars.cards.Sett;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class TradingBlow extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(TradingBlow.class.getSimpleName());
    public static final String IMG = makeCardPath("TradingBlow.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public TradingBlow() {
        super(ID, IMG, 0, CardType.SKILL, SettTheBoss.Enums.COLOR_GRAY, CardRarity.COMMON, CardTarget.SELF);
        this.baseMagicNumber = 5;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        p.currentHealth -= magicNumber;

    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.exhaust = false;
            this.rawDescription = cardStrings.UPGRADE_DESCRIPTION;
            initializeDescription();
        }

    }

}
