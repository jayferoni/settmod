package leagueChars.cards.Sett;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LearnW extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(LearnW.class.getSimpleName());
    public static final String IMG = makeCardPath("HaymakerLearn.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);



    public LearnW() {
        super(ID,IMG,1,CardType.SKILL, SettTheBoss.Enums.COLOR_GRAY,CardRarity.UNCOMMON,CardTarget.SELF);
      this.exhaust=true;
    }
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        AbstractCard c = new Haymaker() ;
        c.setCostForTurn(0);
        this.addToBot(new MakeTempCardInHandAction(c, 1));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(0);

        }

    }



}
