package leagueChars.cards.Sett;

import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DoubleTapPower;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class LeftRightCombo extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(LeftRightCombo.class.getSimpleName());
    public static final String IMG = makeCardPath("LeftRight.png");
    public static final CardStrings CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(ID);

    public LeftRightCombo() {
        super(ID, IMG, 1, CardType.SKILL, SettTheBoss.Enums.COLOR_GRAY, CardRarity.RARE, CardTarget.SELF);
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        {
            this.addToBot(new ApplyPowerAction(p, p, new DoubleTapPower(p, this.magicNumber), this.magicNumber));
        }
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(1);
            this.rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
            initializeDescription();
        }

    }

}
