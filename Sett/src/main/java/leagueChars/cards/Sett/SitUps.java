package leagueChars.cards.Sett;

import com.megacrit.cardcrawl.actions.utility.SFXAction;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.status.Wound;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;
import com.megacrit.cardcrawl.powers.StrengthPower;
import com.megacrit.cardcrawl.rooms.AbstractRoom;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class SitUps extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(SitUps.class.getSimpleName());
    public static final String IMG = makeCardPath("SitUps.png");


    public SitUps() {
        super(ID,IMG,2,CardType.SKILL, SettTheBoss.Enums.COLOR_GRAY,CardRarity.RARE,CardTarget.SELF);
      this.baseMagicNumber =3;
      this.exhaust =true;
        this.magicNumber = this.baseMagicNumber;
    }
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        this.addToBot(new SFXAction("DefendSound"));


            this.addToBot(new ApplyPowerAction(p, p, new StrengthPower(p, this.magicNumber), this.magicNumber));
        }


    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeMagicNumber(1);
        }

    }



}
