package leagueChars.cards.Sett;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class AgainsttheOdds extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(AgainsttheOdds.class.getSimpleName());
    public static final String IMG = makeCardPath("AgainsttheOdds.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public AgainsttheOdds() {
        super(ID, IMG, 1, CardType.SKILL, SettTheBoss.Enums.COLOR_GRAY, CardRarity.UNCOMMON, CardTarget.SELF);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        // Empty, because handled by relic HeartOfTheHalfBeast and FullPower
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(0);
        }

    }



}
