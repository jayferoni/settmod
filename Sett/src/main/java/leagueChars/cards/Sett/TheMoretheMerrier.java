package leagueChars.cards.Sett;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.status.Wound;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.RegenPower;
import com.megacrit.cardcrawl.powers.StrengthPower;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import leagueChars.LeagueCharsMod;
import leagueChars.cards.AbstractDynamicCard;
import leagueChars.characters.SettTheBoss;

import static leagueChars.LeagueCharsMod.makeCardPath;

public class TheMoretheMerrier extends AbstractDynamicCard {
    public static final String ID = LeagueCharsMod.makeID(TheMoretheMerrier.class.getSimpleName());
    public static final String IMG = makeCardPath("TheMoreTheMerrier.png");

    public TheMoretheMerrier() {
        super(ID, IMG, 1, CardType.SKILL, SettTheBoss.Enums.COLOR_GRAY, CardRarity.UNCOMMON, CardTarget.SELF);
        this.baseMagicNumber = 1;
        this.exhaust=true;

    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        int numOfAliveMonsters = AbstractDungeon.getCurrRoom().monsters.monsters.stream()
                .filter(monster -> !monster.isDeadOrEscaped()).toArray().length;
        this.addToBot(new ApplyPowerAction(p, p, new StrengthPower(p, numOfAliveMonsters), numOfAliveMonsters));
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBaseCost(0);
        }

    }

}
