package leagueChars.vfx;

import leagueChars.LeagueCharsMod;
import leagueChars.util.TextureLoader;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.vfx.AbstractGameEffect;

public class HaymakerEffect extends AbstractGameEffect {
    private float x;
    private float y;
    private float vX;
    private static final float FADE_IN_TIME = 0.05F;
    private static final float FADE_OUT_TIME = 0.4F;
    private float fadeInTimer = 0F;
    private float fadeOutTimer = 1.0F;
    private float stallTimer;
    private final Texture img;

    public HaymakerEffect() {
        this.img = TextureLoader.getTexture(LeagueCharsMod.getModID() + "Resources/images/vfx/SettVisualMax2.png");
        this.color = new Color(1.0F, 1.0F, 1.0F, 0.0F);
        this.x = (float) Settings.WIDTH * 0.4F - (float) this.img.getWidth() / 2.0F;
        this.y = AbstractDungeon.floorY + 100.0F * Settings.scale - (float) this.img.getHeight() / 2.0F;
        this.vX = ((float) Settings.WIDTH * 0.4F) * Settings.scale;
        this.stallTimer = MathUtils.random(0.0F, 0.2F);
        this.scale = 2.0F * Settings.scale;
        this.rotation = MathUtils.random(-5.0F, 1.0F);

    }

    public void update() {
        if (this.stallTimer > 0.0F) {
            this.stallTimer -= Gdx.graphics.getDeltaTime();
        } else {
            this.x += this.vX * Gdx.graphics.getDeltaTime();
            this.rotation += MathUtils.random(-0.5F, 0.5F);
            this.scale += 0.005F * Settings.scale;
            if (this.fadeInTimer != 0.0F) {
                this.fadeInTimer -= Gdx.graphics.getDeltaTime();
                if (this.fadeInTimer < 0.0F) {
                    this.fadeInTimer = 0.0F;
                }

                this.color.a = Interpolation.fade.apply(1.0F, 0.0F, this.fadeInTimer / 0.05F);
            } else if (this.fadeOutTimer != 0.0F) {
                this.fadeOutTimer -= Gdx.graphics.getDeltaTime();
                if (this.fadeOutTimer < 0.0F) {
                    this.fadeOutTimer = 0.0F;
                }

                this.color.a = Interpolation.pow2.apply(0.0F, 1.0F, this.fadeOutTimer / 0.4F);
            } else {
                this.isDone = true;

            }

        }
    }

    public void render(SpriteBatch sb) {
        sb.setColor(this.color);
        sb.draw(this.img, this.x, this.y);
    }

    public void dispose() {
    }
}
