package leagueChars.util;

public interface AttackCritSubscriber {
    public void onCrit(boolean wasACrit);
}
