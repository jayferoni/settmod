//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package leagueChars.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.unlock.UnlockTracker;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

public class WarmogsAction extends AbstractGameAction {
    private DamageInfo info;
    private int increaseHpAmount;

    public WarmogsAction(AbstractCreature target, DamageInfo info,  int maxHPAmount) {
        this.actionType = ActionType.BLOCK;
        this.target = target;
        this.info = info;

        this.increaseHpAmount = maxHPAmount;
    }

    public void update() {
        AbstractDungeon.player.increaseMaxHp(this.increaseHpAmount, false);


        this.isDone = true;
    }
}
