package leagueChars;

import basemod.*;
import basemod.eventUtil.AddEventParams;
import basemod.interfaces.*;
import leagueChars.cardPools.AbilityPower;
import leagueChars.cardPools.AttackDamage;
import leagueChars.cardPools.Nexus;
import leagueChars.cards.*;
import leagueChars.characters.Mordekaiser;
import leagueChars.characters.SettTheBoss;
import leagueChars.characters.Viktor;
import leagueChars.characters.Tryndamere;
import leagueChars.characters.Yasuo;
import leagueChars.events.IdentityCrisisEvent;

import leagueChars.potions.GritPotion;
import leagueChars.relics.*;
import leagueChars.util.IDCheckDontTouchPls;
import leagueChars.util.TextureLoader;
import leagueChars.variables.AbilityPowerVariable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.evacipated.cardcrawl.mod.stslib.Keyword;
import com.evacipated.cardcrawl.modthespire.lib.SpireConfig;
import com.evacipated.cardcrawl.modthespire.lib.SpireInitializer;
import com.google.gson.Gson;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.TheCity;
import com.megacrit.cardcrawl.helpers.CardHelper;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.localization.*;
import com.megacrit.cardcrawl.unlock.UnlockTracker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/*
 * With that out of the way:
 * Welcome to this super over-commented Slay the Spire modding base.
 * Use it to make your own mod of any type. - If you want to add any standard in-game content (character,
 * cards, relics), this is a good starting point.
 * It features 1 character with a minimal set of things: 1 card of each type, 1 debuff, couple of relics, etc.
 * If you're new to modding, you basically *need* the BaseMod wiki for whatever you wish to add
 * https://github.com/daviscook477/BaseMod/wiki - work your way through with this base.
 * Feel free to use this in any way you like, of course. MIT licence applies. Happy modding!
 *
 * And pls. Read the comments.
 */

@SpireInitializer
public class LeagueCharsMod implements EditCardsSubscriber, AddAudioSubscriber, EditRelicsSubscriber,
        EditStringsSubscriber, EditKeywordsSubscriber, EditCharactersSubscriber, PostInitializeSubscriber {

    // Make sure to implement the subscribers *you* are using (read basemod wiki).
    // Editing cards? EditCardsSubscriber.
    // Making relics? EditRelicsSubscriber. etc., etc., for a full list and how to
    // make your own, visit the basemod wiki.
    public static final Logger logger = LogManager.getLogger(LeagueCharsMod.class.getName());
    private static String modID;

    // Mod-settings settings. This is if you want an on/off savable button
    public static Properties theDefaultDefaultSettings = new Properties();
    public static final String ENABLE_PLACEHOLDER_SETTINGS = "enablePlaceholder";
    public static boolean enablePlaceholder = true; // The boolean we'll be setting on/off (true/false)

    // This is for the in-game mod settings panel.
    private static final String MODNAME = "League Characters Mod";
    private static final String AUTHOR = "JayFeroni, kasokz, monomo"; // And pretty soon - You!
    private static final String DESCRIPTION = "A League of Legends Characters Mod for Slay the Spire";

    // =============== INPUT TEXTURE LOCATION =================

    // Colors (RGB)
    // Character Color
    public static final Color DEFAULT_GRAY = CardHelper.getColor(64.0f, 70.0f, 70.0f);
    public static final Color NEXUS_GRAY = CardHelper.getColor(64.0f, 70.0f, 70.0f);
    public static final Color VIKTOR_DARK_RED = CardHelper.getColor(139f, 0f, 0f);

    // Potion Colors in RGB
    public static final Color PLACEHOLDER_POTION_LIQUID = CardHelper.getColor(209.0f, 53.0f, 18.0f); // Orange-ish Red
    public static final Color PLACEHOLDER_POTION_HYBRID = CardHelper.getColor(255.0f, 230.0f, 230.0f); // Near White
    public static final Color PLACEHOLDER_POTION_SPOTS = CardHelper.getColor(100.0f, 25.0f, 10.0f); // Super Dark
    // Red/Brown

    // ONCE YOU CHANGE YOUR MOD ID (BELOW, YOU CAN'T MISS IT) CHANGE THESE
    // PATHS!!!!!!!!!!!
    // ONCE YOU CHANGE YOUR MOD ID (BELOW, YOU CAN'T MISS IT) CHANGE THESE
    // PATHS!!!!!!!!!!!
    // ONCE YOU CHANGE YOUR MOD ID (BELOW, YOU CAN'T MISS IT) CHANGE THESE
    // PATHS!!!!!!!!!!!
    // ONCE YOU CHANGE YOUR MOD ID (BELOW, YOU CAN'T MISS IT) CHANGE THESE
    // PATHS!!!!!!!!!!!
    // ONCE YOU CHANGE YOUR MOD ID (BELOW, YOU CAN'T MISS IT) CHANGE THESE
    // PATHS!!!!!!!!!!!
    // ONCE YOU CHANGE YOUR MOD ID (BELOW, YOU CAN'T MISS IT) CHANGE THESE
    // PATHS!!!!!!!!!!!

    // Card backgrounds - The actual rectangular card.
    private static final String ATTACK_DEFAULT_GRAY = "leagueCharsResources/images/512/bg_attack_default_gray.png";
    private static final String SKILL_DEFAULT_GRAY = "leagueCharsResources/images/512/bg_skill_default_gray.png";
    private static final String POWER_DEFAULT_GRAY = "leagueCharsResources/images/512/bg_power_default_gray.png";
    private static final String ATTACK_VIKTOR_DARK_RED = "leagueCharsResources/images/512/bg_attack_viktor_dark_red.png";
    private static final String SKILL_VIKTOR_DARK_RED = "leagueCharsResources/images/512/bg_skill_viktor_dark_red.png";
    private static final String POWER_VIKTOR_DARK_RED = "leagueCharsResources/images/512/bg_power_viktor_dark_red.png";

    private static final String ENERGY_ORB_DEFAULT_GRAY = "leagueCharsResources/images/512/card_default_gray_orb.png";
    private static final String CARD_ENERGY_ORB = "leagueCharsResources/images/512/card_small_orb.png";

    private static final String ATTACK_DEFAULT_GRAY_PORTRAIT = "leagueCharsResources/images/1024/bg_attack_default_gray.png";
    private static final String SKILL_DEFAULT_GRAY_PORTRAIT = "leagueCharsResources/images/1024/bg_skill_default_gray.png";
    private static final String POWER_DEFAULT_GRAY_PORTRAIT = "leagueCharsResources/images/1024/bg_power_default_gray.png";
    private static final String ATTACK_VIKTOR_DARK_RED_PORTRAIT = "leagueCharsResources/images/1024/bg_attack_viktor_dark_red.png";
    private static final String SKILL_VIKTOR_DARK_RED_PORTRAIT = "leagueCharsResources/images/1024/bg_skill_viktor_dark_red.png";
    private static final String POWER_VIKTOR_DARK_RED_PORTRAIT = "leagueCharsResources/images/1024/bg_power_viktor_dark_red.png";
    private static final String ENERGY_ORB_DEFAULT_GRAY_PORTRAIT = "leagueCharsResources/images/1024/card_default_gray_orb.png";

    // Character assets
    private static final String SETT_BUTTON = "leagueCharsResources/images/charSelect/SettButton.png";
    private static final String SETT_PORTRAIT = "leagueCharsResources/images/charSelect/SettBG.png";
    public static final String SETT_MODEL = "leagueCharsResources/images/char/Sett/model.png";
    public static final String SETT_SHOULDER_1 = "leagueCharsResources/images/char/Sett/shoulder.png";
    public static final String SETT_SHOULDER_2 = "leagueCharsResources/images/char/Sett/shoulder2.png";
    public static final String SETT_CORPSE = "leagueCharsResources/images/char/Sett/corpse.png";
    // Mordekaiser
    private static final String MORDE_BUTTON = "leagueCharsResources/images/charSelect/MordeButton.png";
    private static final String MORDE_PORTRAIT = "leagueCharsResources/images/charSelect/MordeBG.png";
    public static final String MORDE_MODEL = "leagueCharsResources/images/char/Mordekaiser/model.png";
    public static final String MORDE_SHOULDER_1 = "leagueCharsResources/images/char/Mordekaiser/shoulder.png";
    public static final String MORDE_SHOULDER_2 = "leagueCharsResources/images/char/Mordekaiser/shoulder.png";
    public static final String MORDE_CORPSE = "leagueCharsResources/images/char/Mordekaiser/corpse.png";
    // Viktor
    private static final String VIKTOR_BUTTON = "leagueCharsResources/images/charSelect/ViktorButton.png";
    private static final String VIKTOR_PORTRAIT = "leagueCharsResources/images/charSelect/ViktorBG.png";
    public static final String VIKTOR_MODEL = "leagueCharsResources/images/char/Viktor/model.png";
    public static final String VIKTOR_SHOULDER_1 = "leagueCharsResources/images/char/Viktor/shoulder.png";
    public static final String VIKTOR_SHOULDER_2 = "leagueCharsResources/images/char/Viktor/shoulder.png";
    public static final String VIKTOR_CORPSE = "leagueCharsResources/images/char/Viktor/corpse.png";
    // Tryndamere
    private static final String TRYNDAMERE_BUTTON = "leagueCharsResources/images/charSelect/TryndamereButton.png";
    private static final String TRYNDAMERE_PORTRAIT = "leagueCharsResources/images/charSelect/TryndamereBG.png";
    public static final String TRYNDAMERE_MODEL = "leagueCharsResources/images/char/Tryndamere/model.png";
    public static final String TRYNDAMERE_SHOULDER_1 = "leagueCharsResources/images/char/Tryndamere/shoulder.png";
    public static final String TRYNDAMERE_SHOULDER_2 = "leagueCharsResources/images/char/Tryndamere/shoulder.png";
    public static final String TRYNDAMERE_CORPSE = "leagueCharsResources/images/char/Tryndamere/corpse.png";
    // Yasuo
    private static final String YASUO_BUTTON = "leagueCharsResources/images/charSelect/YasuoButton.png";
    private static final String YASUO_PORTRAIT = "leagueCharsResources/images/charSelect/YasuoBG.png";
    public static final String YASUO_MODEL = "leagueCharsResources/images/char/Yasuo/model.png";
    public static final String YASUO_SHOULDER_1 = "leagueCharsResources/images/char/Yasuo/shoulder.png";
    public static final String YASUO_SHOULDER_2 = "leagueCharsResources/images/char/Yasuo/shoulder.png";
    public static final String YASUO_CORPSE = "leagueCharsResources/images/char/Yasuo/corpse.png";

    // Mod Badge - A small icon that appears in the mod settings menu next to your
    // mod.
    public static final String BADGE_IMAGE = "leagueCharsResources/images/Badge.png";

    // Atlas and JSON files for the Animations
    public static final String THE_DEFAULT_SKELETON_ATLAS = "leagueCharsResources/images/char/defaultCharacter/skeleton.atlas";
    public static final String THE_DEFAULT_SKELETON_JSON = "leagueCharsResources/images/char/defaultCharacter/skeleton.json";

    // =============== MAKE IMAGE PATHS =================

    public static String assetPath(String path) {
        return "leagueCharsResources/" + path;
    }

    public static String makeCardPath(String resourcePath) {
        return getModID() + "Resources/images/cards/" + resourcePath;
    }

    public static String makeSoundPath(String resourcePath) {
        return getModID() + "Resources/audio/sfx/" + resourcePath;
    }

    public static String makeRelicPath(String resourcePath) {
        return getModID() + "Resources/images/relics/" + resourcePath;
    }

    public static String makeRelicOutlinePath(String resourcePath) {
        return getModID() + "Resources/images/relics/outline/" + resourcePath;
    }

    public static String makeOrbPath(String resourcePath) {
        return getModID() + "Resources/images/orbs/" + resourcePath;
    }

    public static String makePowerPath(String resourcePath) {
        return getModID() + "Resources/images/powers/" + resourcePath;
    }

    public static String makeEventPath(String resourcePath) {
        return getModID() + "Resources/images/events/" + resourcePath;
    }

    // =============== /MAKE IMAGE PATHS/ =================

    // =============== /INPUT TEXTURE LOCATION/ =================

    // =============== SUBSCRIBE, CREATE THE COLOR_GRAY, INITIALIZE
    // =================

    public LeagueCharsMod() {
        logger.info("Subscribe to BaseMod hooks");

        BaseMod.subscribe(this);

        setModID("leagueChars");
        // cool
        // TODO: NOW READ THIS!!!!!!!!!!!!!!!:

        // 3. Scroll down (or search for "ADD CARDS") till you reach the ADD CARDS
        // section, and follow the TODO instructions

        // 4. FINALLY and most importantly: Scroll up a bit. You may have noticed the
        // image locations above don't use getModID()
        // Change their locations to reflect your actual ID rather than theDefault. They
        // get loaded before getID is a thing.

        logger.info("Done subscribing");

        logger.info("Creating the color " + SettTheBoss.Enums.COLOR_GRAY.toString());

        // Pool Colors
        BaseMod.addColor(Nexus.Enums.COLOR, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);
        BaseMod.addColor(AbilityPower.Enums.COLOR, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);
        BaseMod.addColor(AttackDamage.Enums.COLOR, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);

        // Character Colors
        BaseMod.addColor(SettTheBoss.Enums.COLOR_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);
        BaseMod.addColor(Tryndamere.Enums.COLOR, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);
        BaseMod.addColor(Viktor.Enums.COLOR_DARK_RED, VIKTOR_DARK_RED, VIKTOR_DARK_RED, VIKTOR_DARK_RED,
                VIKTOR_DARK_RED, VIKTOR_DARK_RED, VIKTOR_DARK_RED, VIKTOR_DARK_RED, ATTACK_VIKTOR_DARK_RED,
                SKILL_VIKTOR_DARK_RED, POWER_VIKTOR_DARK_RED, ENERGY_ORB_DEFAULT_GRAY, ATTACK_VIKTOR_DARK_RED_PORTRAIT,
                SKILL_VIKTOR_DARK_RED_PORTRAIT, POWER_VIKTOR_DARK_RED_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT,
                CARD_ENERGY_ORB);
        BaseMod.addColor(Mordekaiser.Enums.COLOR_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);
        BaseMod.addColor(Yasuo.Enums.COLOR_DARK_RED, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY,
                DEFAULT_GRAY, DEFAULT_GRAY, DEFAULT_GRAY, ATTACK_DEFAULT_GRAY, SKILL_DEFAULT_GRAY, POWER_DEFAULT_GRAY,
                ENERGY_ORB_DEFAULT_GRAY, ATTACK_DEFAULT_GRAY_PORTRAIT, SKILL_DEFAULT_GRAY_PORTRAIT,
                POWER_DEFAULT_GRAY_PORTRAIT, ENERGY_ORB_DEFAULT_GRAY_PORTRAIT, CARD_ENERGY_ORB);

        logger.info("Done creating the color");

        logger.info("Adding mod settings");
        // This loads the mod settings.
        // The actual mod Button is added below in receivePostInitialize()
        theDefaultDefaultSettings.setProperty(ENABLE_PLACEHOLDER_SETTINGS, "FALSE"); // This is the default setting.
        // It's actually set...
        try {
            SpireConfig config = new SpireConfig("defaultMod", "theDefaultConfig", theDefaultDefaultSettings); // ...right
            // here
            // the "fileName" parameter is the name of the file MTS will create where it
            // will save our setting.
            config.load(); // Load the setting and set the boolean to equal it
            enablePlaceholder = config.getBool(ENABLE_PLACEHOLDER_SETTINGS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("Done adding mod settings");

    }

    // ====== NO EDIT AREA ======
    // DON'T TOUCH THIS STUFF. IT IS HERE FOR STANDARDIZATION BETWEEN MODS AND TO
    // ENSURE GOOD CODE PRACTICES.
    // IF YOU MODIFY THIS I WILL HUNT YOU DOWN AND DOWNVOTE YOUR MOD ON WORKSHOP

    public static void setModID(String ID) { // DON'T EDIT
        Gson coolG = new Gson(); // EY DON'T EDIT THIS
        // String IDjson =
        // Gdx.files.internal("IDCheckStringsDONT-EDIT-AT-ALL.json").readString(String.valueOf(StandardCharsets.UTF_8));
        // // i hate u Gdx.files
        InputStream in = LeagueCharsMod.class.getResourceAsStream("/IDCheckStringsDONT-EDIT-AT-ALL.json"); // DON'T EDIT
        // THIS ETHER
        IDCheckDontTouchPls EXCEPTION_STRINGS = coolG.fromJson(new InputStreamReader(in, StandardCharsets.UTF_8),
                IDCheckDontTouchPls.class); // OR THIS, DON'T EDIT IT
        logger.info("You are attempting to set your mod ID as: " + ID); // NO WHY
        if (ID.equals(EXCEPTION_STRINGS.DEFAULTID)) { // DO *NOT* CHANGE THIS ESPECIALLY, TO EDIT YOUR MOD ID, SCROLL UP
            // JUST A LITTLE, IT'S JUST ABOVE
            throw new RuntimeException(EXCEPTION_STRINGS.EXCEPTION); // THIS ALSO DON'T EDIT
        } else if (ID.equals(EXCEPTION_STRINGS.DEVID)) { // NO
            modID = EXCEPTION_STRINGS.DEFAULTID; // DON'T
        } else { // NO EDIT AREA
            modID = ID; // DON'T WRITE OR CHANGE THINGS HERE NOT EVEN A LITTLE
        } // NO
        logger.info("Success! ID is " + modID); // WHY WOULD U WANT IT NOT TO LOG?? DON'T EDIT THIS.
    } // NO

    public static String getModID() { // NO
        return modID; // DOUBLE NO
    } // NU-UH

    private static void pathCheck() { // ALSO NO
        Gson coolG = new Gson(); // NOPE DON'T EDIT THIS
        // String IDjson =
        // Gdx.files.internal("IDCheckStringsDONT-EDIT-AT-ALL.json").readString(String.valueOf(StandardCharsets.UTF_8));
        // // i still hate u btw Gdx.files
        InputStream in = LeagueCharsMod.class.getResourceAsStream("/IDCheckStringsDONT-EDIT-AT-ALL.json"); // DON'T EDIT
        // THISSSSS
        IDCheckDontTouchPls EXCEPTION_STRINGS = coolG.fromJson(new InputStreamReader(in, StandardCharsets.UTF_8),
                IDCheckDontTouchPls.class); // NAH, NO EDIT
        String packageName = LeagueCharsMod.class.getPackage().getName(); // STILL NO EDIT ZONE
        FileHandle resourcePathExists = Gdx.files.internal(getModID() + "Resources"); // PLEASE DON'T EDIT THINGS HERE,
        // THANKS
        if (!modID.equals(EXCEPTION_STRINGS.DEVID)) { // LEAVE THIS EDIT-LESS
            if (!packageName.equals(getModID())) { // NOT HERE ETHER
                throw new RuntimeException(EXCEPTION_STRINGS.PACKAGE_EXCEPTION + getModID()); // THIS IS A NO-NO
            } // WHY WOULD U EDIT THIS
            if (!resourcePathExists.exists()) { // DON'T CHANGE THIS
                throw new RuntimeException(EXCEPTION_STRINGS.RESOURCE_FOLDER_EXCEPTION + getModID() + "Resources"); // NOT
                // THIS
            } // NO
        } // NO
    }// NO

    // ====== YOU CAN EDIT AGAIN ======

    public static void initialize() {
        logger.info("========================= Initializing Default Mod. Hi. =========================");
        LeagueCharsMod defaultmod = new LeagueCharsMod();
        logger.info("========================= /Default Mod Initialized. Hello World./ =========================");
    }

    // ============== /SUBSCRIBE, CREATE THE COLOR_GRAY, INITIALIZE/
    // =================

    // =============== LOAD THE CHARACTER =================

    @Override
    public void receiveEditCharacters() {

        BaseMod.addCharacter(new Tryndamere("Tryndamere", Tryndamere.Enums.TRYNDAMERE), TRYNDAMERE_BUTTON,
                TRYNDAMERE_PORTRAIT, Tryndamere.Enums.TRYNDAMERE);
        BaseMod.addCharacter(new SettTheBoss("Sett", SettTheBoss.Enums.SETT), SETT_BUTTON, SETT_PORTRAIT,
                SettTheBoss.Enums.SETT);
        BaseMod.addCharacter(new Viktor("Viktor", Viktor.Enums.VIKTOR), VIKTOR_BUTTON, VIKTOR_PORTRAIT,
                Viktor.Enums.VIKTOR);
        BaseMod.addCharacter(new Mordekaiser("Mordekaiser", Mordekaiser.Enums.MORDEKAISER), MORDE_BUTTON,
                MORDE_PORTRAIT, Mordekaiser.Enums.MORDEKAISER);
        BaseMod.addCharacter(new Yasuo("Yasuo", Yasuo.Enums.YASUO), YASUO_BUTTON,
                YASUO_PORTRAIT, Yasuo.Enums.YASUO);
        logger.info("Added " + SettTheBoss.Enums.SETT.toString());
    }

    // =============== /LOAD THE CHARACTER/ =================

    // =============== POST-INITIALIZE =================

    @Override
    public void receivePostInitialize() {
        logger.info("Loading badge image and mod options");

        // Load the Mod Badge
        Texture badgeTexture = TextureLoader.getTexture(BADGE_IMAGE);

        // Create the Mod Menu
        ModPanel settingsPanel = new ModPanel();

        // Create the on/off button:
        ModLabeledToggleButton enableNormalsButton = new ModLabeledToggleButton(
                "This is the text which goes next to the checkbox.", 350.0f, 700.0f, Settings.CREAM_COLOR,
                FontHelper.charDescFont, // Position (trial and error it), color, font
                enablePlaceholder, // Boolean it uses
                settingsPanel, // The mod panel in which this button will be in
                (label) -> {
                }, // thing??????? idk
                (button) -> { // The actual button:

                    enablePlaceholder = button.enabled; // The boolean true/false will be whether the button is enabled
                    // or not
                    try {
                        // And based on that boolean, set the settings and save them
                        SpireConfig config = new SpireConfig("defaultMod", "theDefaultConfig",
                                theDefaultDefaultSettings);
                        config.setBool(ENABLE_PLACEHOLDER_SETTINGS, enablePlaceholder);
                        config.save();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        settingsPanel.addUIElement(enableNormalsButton); // Add the button to the settings panel. Button is a go.

        BaseMod.registerModBadge(badgeTexture, MODNAME, AUTHOR, DESCRIPTION, settingsPanel);

        // =============== EVENTS =================
        // https://github.com/daviscook477/BaseMod/wiki/Custom-Events

        // You can add the event like so:
        // BaseMod.addEvent(IdentityCrisisEvent.ID, IdentityCrisisEvent.class,
        // TheCity.ID);
        // Then, this event will be exclusive to the City (act 2), and will show up for
        // all characters.
        // If you want an event that's present at any part of the game, simply don't
        // include the dungeon ID

        // If you want to have more specific event spawning (e.g. character-specific or
        // so)
        // deffo take a look at that basemod wiki link as well, as it explains things
        // very in-depth
        // btw if you don't provide event type, normal is assumed by default

        // Create a new event builder
        // Since this is a builder these method calls (outside of create()) can be
        // skipped/added as necessary
        AddEventParams eventParams = new AddEventParams.Builder(IdentityCrisisEvent.ID, IdentityCrisisEvent.class) // for
                // this
                // specific
                // event
                .dungeonID(TheCity.ID) // The dungeon (act) this event will appear in
                .playerClass(SettTheBoss.Enums.SETT) // Character specific event
                .create();

        // Add the event
        BaseMod.addEvent(eventParams);

        receiveEditPotions();

        // =============== /EVENTS/ =================
        logger.info("Done loading badge Image and mod options");
    }

    // =============== / POST-INITIALIZE/ =================

    // ================ ADD POTIONS ===================

    public void receiveEditPotions() {
        logger.info("Beginning to edit potions");

        // Class Specific Potion. If you want your potion to not be class-specific,
        // just remove the player class at the end (in this case the
        // "TheDefaultEnum.THE_DEFAULT".
        // Remember, you can press ctrl+P inside parentheses like addPotions)
        BaseMod.addPotion(GritPotion.class, PLACEHOLDER_POTION_LIQUID, PLACEHOLDER_POTION_HYBRID,
                PLACEHOLDER_POTION_SPOTS, GritPotion.POTION_ID, SettTheBoss.Enums.SETT);

        logger.info("Done editing potions");
    }

    // ================ /ADD POTIONS/ ===================

    // ================ ADD RELICS ===================

    @Override
    public void receiveEditRelics() {
        logger.info("Adding relics");

        // Take a look at https://github.com/daviscook477/BaseMod/wiki/AutoAdd
        // as well as
        // https://github.com/kiooeht/Bard/blob/e023c4089cc347c60331c78c6415f489d19b6eb9/src/main/java/com/evacipated/cardcrawl/mod/bard/BardMod.java#L319
        // for reference as to how to turn this into an "Auto-Add" rather than having to
        // list every relic individually.
        // Of note is that the bard mod uses it's own custom relic class (not dissimilar
        // to our AbstractDefaultCard class for cards) that adds the 'color' field,
        // in order to automatically differentiate which pool to add the relic too.
        BaseMod.addRelicToCustomPool(new DarknessRise(), Mordekaiser.Enums.COLOR_GRAY);
        BaseMod.addRelicToCustomPool(new HeartoftheHalfBeast(), SettTheBoss.Enums.COLOR_GRAY);
        BaseMod.addRelicToCustomPool(new WinnerofthePit(), SettTheBoss.Enums.COLOR_GRAY);
        BaseMod.addRelicToCustomPool(new FullPower(), SettTheBoss.Enums.COLOR_GRAY);
        BaseMod.addRelicToCustomPool(new GloriousEvolution(), Viktor.Enums.COLOR_DARK_RED);
        BaseMod.addRelicToCustomPool(new BattleFury(), Tryndamere.Enums.COLOR);
        BaseMod.addRelicToCustomPool(new DarkSeal(), AbilityPower.Enums.COLOR);
        BaseMod.addRelicToCustomPool(new WayOfTheWanderer(), Yasuo.Enums.COLOR_DARK_RED);


        UnlockTracker.markRelicAsSeen(HeartoftheHalfBeast.ID);
        UnlockTracker.markRelicAsSeen(WinnerofthePit.ID);
        UnlockTracker.markRelicAsSeen(FullPower.ID);
        UnlockTracker.markRelicAsSeen(GloriousEvolution.ID);
        UnlockTracker.markRelicAsSeen(DarknessRise.ID);
        UnlockTracker.markRelicAsSeen(BattleFury.ID);
        UnlockTracker.markRelicAsSeen(DarkSeal.ID);
        UnlockTracker.markRelicAsSeen(WayOfTheWanderer.ID);
        logger.info("Done adding relics!");
    }

    // ================ /ADD RELICS/ ===================

    // ================ ADD CARDS ===================

    @Override
    public void receiveEditCards() {
        logger.info("Adding variables");
        // Ignore this
        pathCheck();
        // Add the Custom Dynamic Variables
        logger.info("Add variables");
        // Add the Custom Dynamic variables
        BaseMod.addDynamicVariable(new AbilityPowerVariable());

        logger.info("Adding cards");
        // Add the cards
        // Don't delete these default cards yet. You need 1 of each type and rarity
        // (technically) for your game not to crash
        // when generating card rewards/shop screen items.

        // This method automatically adds any cards so you don't have to manually load
        // them 1 by 1
        // For more specific info, including how to exclude cards from being added:
        // https://github.com/daviscook477/BaseMod/wiki/AutoAdd

        // The ID for this function isn't actually your modid as used for prefixes/by
        // the getModID() method.
        // It's the mod id you give MTS in ModTheSpire.json - by default your artifact
        // ID in your pom.xml

        new AutoAdd("leagueChars") // ${project.artifactId}
                .packageFilter(AbstractDefaultCard.class) // filters to any class in the same package as
                // AbstractDefaultCard, nested packages included
                .setDefaultSeen(true).cards();

        // .setDefaultSeen(true) unlocks the cards
        // This is so that they are all "seen" in the library,
        // for people who like to look at the card list before playing your mod

        logger.info("Done adding cards!");
    }

    // ================ /ADD CARDS/ ===================
    private static String makeLocPath(Settings.GameLanguage language, String filename) {
        String ret = "localization/";
        switch (language) {
            case ZHS:
                ret += "zhs/";
                break;
            default:
                ret += "eng/";
                break;
        }
        return assetPath(ret + filename + ".json");
    }

    private void loadLocFiles(Settings.GameLanguage language) {
        BaseMod.loadCustomStringsFile(CardStrings.class, makeLocPath(language, "LeagueCharsMod-Card-Strings"));
        BaseMod.loadCustomStringsFile(RelicStrings.class, makeLocPath(language, "LeagueCharsMod-Relic-Strings"));
        BaseMod.loadCustomStringsFile(PotionStrings.class, makeLocPath(language, "LeagueCharsMod-Potion-Strings"));
        BaseMod.loadCustomStringsFile(PowerStrings.class, makeLocPath(language, "LeagueCharsMod-Power-Strings"));
        BaseMod.loadCustomStringsFile(CharacterStrings.class,
                makeLocPath(language, "LeagueCharsMod-Character-Strings"));
        BaseMod.loadCustomStringsFile(EventStrings.class, makeLocPath(language, "LeagueCharsMod-Event-Strings"));
    }
    // ================ LOAD THE TEXT ===================

    @Override
    public void receiveEditStrings() {
        loadLocFiles(Settings.GameLanguage.ENG);
        if (Settings.language != Settings.GameLanguage.ENG) {
            loadLocFiles(Settings.language);
        }
    }
    // ================ /LOAD THE TEXT/ ===================

    // ================ LOAD THE KEYWORDS ===================

    @Override
    public void receiveEditKeywords() {
        // Keywords on cards are supposed to be Capitalized, while in
        // Keyword-String.json they're lowercase
        //
        // Multiword keywords on cards are done With_Underscores
        //
        // If you're using multiword keywords, the first element in your NAMES array in
        // your keywords-strings.json has to be the same as the PROPER_NAME.
        // That is, in Card-Strings.json you would have #yA_Long_Keyword (#y highlights
        // the keyword in yellow).
        // In Keyword-Strings.json you would have PROPER_NAME as A Long Keyword and the
        // first element in NAMES be a long keyword, and the second element be
        // a_long_keyword

        Gson gson = new Gson();
        String json = Gdx.files.internal(getModID() + "Resources/localization/eng/LeagueCharsMod-Keyword-Strings.json")
                .readString(String.valueOf(StandardCharsets.UTF_8));
        com.evacipated.cardcrawl.mod.stslib.Keyword[] keywords = gson.fromJson(json,
                com.evacipated.cardcrawl.mod.stslib.Keyword[].class);

        if (keywords != null) {
            for (Keyword keyword : keywords) {
                BaseMod.addKeyword(getModID().toLowerCase(), keyword.PROPER_NAME, keyword.NAMES, keyword.DESCRIPTION);
                // getModID().toLowerCase() makes your keyword mod specific (it won't show up in
                // other cards that use that word)
            }
        }
    }

    // ================ /LOAD THE KEYWORDS/ ===================

    // this adds "ModName:" before the ID of any card/relic/power etc.
    // in order to avoid conflicts if any other mod uses the same ID.
    public static String makeID(String idText) {
        return getModID() + ":" + idText;
    }

    @Override
    public void receiveAddAudio() {
        BaseMod.addAudio("HaymakerSound", makeSoundPath("HaymakerSound.ogg"));
        BaseMod.addAudio("KnuckleDownSound", makeSoundPath("KnuckleDownSound.ogg"));
        BaseMod.addAudio("FacebreakerSound", makeSoundPath("FacebreakerSound.ogg"));
        BaseMod.addAudio("ShowStopperSound", makeSoundPath("ShowStopperSound.ogg"));
        BaseMod.addAudio("SettIntroSound", makeSoundPath("SettIntroSound.ogg"));
        BaseMod.addAudio("KnuckleDownSoundOld", makeSoundPath("KnuckleDownSoundOld.ogg"));
        BaseMod.addAudio("BossSound", makeSoundPath("BossSound.ogg"));
        BaseMod.addAudio("DefendSound", makeSoundPath("DefendSound.ogg"));
        BaseMod.addAudio("MordeQ", makeSoundPath("MordeQ.ogg"));
        BaseMod.addAudio("MordeW", makeSoundPath("MordeW.ogg"));
        BaseMod.addAudio("MordeWR", makeSoundPath("MordeWRecast.ogg"));
        BaseMod.addAudio("MordeE", makeSoundPath("MordeE.ogg"));
        BaseMod.addAudio("MordeR", makeSoundPath("MordeR.ogg"));
        BaseMod.addAudio("MordeIntro", makeSoundPath("MordeSelect.ogg"));
        BaseMod.addAudio("ViktorSiphonPower", makeSoundPath("ViktorSiphonPower.ogg"));
        BaseMod.addAudio("ViktorGravityField", makeSoundPath("ViktorGravityField.ogg"));
        BaseMod.addAudio("ViktorDeathRay", makeSoundPath("ViktorDeathRay.ogg"));
        BaseMod.addAudio("ViktorChaosStorm", makeSoundPath("ViktorChaosStorm.ogg"));
        BaseMod.addAudio("ViktorDischarge", makeSoundPath("ViktorDischarge.ogg"));
        BaseMod.addAudio("ViktorIntroSound", makeSoundPath("VIntroSound.ogg"));
        BaseMod.addAudio("TryndamereSelect", makeSoundPath("TryndamereSelect.ogg"));
        BaseMod.addAudio("TryndamereR", makeSoundPath("TryndaR.ogg"));
        BaseMod.addAudio("TryndamereW", makeSoundPath("TryndaW.ogg"));
        BaseMod.addAudio("YasuoSelect", makeSoundPath("YasuoSelect.ogg"));
        BaseMod.addAudio("YasuoQ1", makeSoundPath("YasuoQ1.ogg"));
        BaseMod.addAudio("YasuoQ2", makeSoundPath("YasuoQ2.ogg"));
        BaseMod.addAudio("YasuoQ3", makeSoundPath("YasuoQ3.ogg"));
        BaseMod.addAudio("YasuoW", makeSoundPath("YasuoW.ogg"));
        BaseMod.addAudio("YasuoE", makeSoundPath("YasuoE.ogg"));
        BaseMod.addAudio("YasuoR", makeSoundPath("YasuoR.ogg"));
        BaseMod.addAudio("Aseryo", makeSoundPath("YasuoAseryo.ogg"));

    }
}
