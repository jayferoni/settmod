package leagueChars.variables;

import basemod.abstracts.DynamicVariable;
import leagueChars.cards.AbstractDefaultCard;

import static leagueChars.LeagueCharsMod.makeID;

import com.megacrit.cardcrawl.cards.AbstractCard;

public class AbilityPowerVariable extends DynamicVariable {

  @Override
  public String key() {
    return makeID("AbilityPower");
  }

  @Override
  public boolean isModified(AbstractCard card) {
    return ((AbstractDefaultCard) card).isAbilityPowerScalingModified;

  }

  @Override
  public int value(AbstractCard card) {
    return ((AbstractDefaultCard) card).abilityPowerScaling;
  }

  @Override
  public int baseValue(AbstractCard card) {
    return ((AbstractDefaultCard) card).baseAbilityPowerScaling;
  }

  @Override
  public boolean upgraded(AbstractCard card) {
    return ((AbstractDefaultCard) card).upgradedAbilityPowerScaling;
  }
}