package leagueChars.cardPools;

import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.helpers.CardLibrary;

public class AbilityPower {
  public static class Enums {
    @SpireEnum
    public static AbstractPlayer.PlayerClass ABILITY_POWER;
    @SpireEnum(name = "AbilityPower")
    public static AbstractCard.CardColor COLOR;
    @SpireEnum(name = "AbilityPower")
    @SuppressWarnings("unused")
    public static CardLibrary.LibraryType LIBRARY;
  }
}
