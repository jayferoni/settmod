package leagueChars.cardPools;

import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.helpers.CardLibrary;

public class AttackDamage {
  public static class Enums {
    @SpireEnum
    public static AbstractPlayer.PlayerClass ATTACK_DAMAGE;
    @SpireEnum(name = "AttackDamage")
    public static AbstractCard.CardColor COLOR;
    @SpireEnum(name = "AttackDamage")
    @SuppressWarnings("unused")
    public static CardLibrary.LibraryType LIBRARY;
  }
}
