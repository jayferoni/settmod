package leagueChars.cardPools;

import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.helpers.CardLibrary;

public class Nexus {
  public static class Enums {
    @SpireEnum
    public static AbstractPlayer.PlayerClass NEXUS;
    @SpireEnum(name = "Nexus")
    public static AbstractCard.CardColor COLOR;
    @SpireEnum(name = "Nexus")
    @SuppressWarnings("unused")
    public static CardLibrary.LibraryType LIBRARY;
  }
}
